import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

from alloniamodel.variables import Variables

from .other_functions import get_factor


class CustomModel(KNeighborsClassifier):
    pass


def zero_arg_func():
    return 2


def one_arg_func(y_pred):
    return 2 * y_pred


def two_arg_func(y_pred, y_real):
    return 2 * y_pred, 2 * y_real


def three_arg_func(y_pred, y_real, z=1):
    return {"a": 2 * y_pred, "b": 2 * y_real, "c": 2 * z}


def three_arg_func_with_x(y_pred, y_real, x):
    return {"a": 2 * y_pred, "b": 2 * y_real, "c": 2 * x}


def feature_engineering_buggy(_):
    raise ValueError("bug!")


def feature_engineering(raw, m=1):
    if isinstance(raw, np.ndarray):
        derived = np.concatenate(
            [raw, m * (raw[:, 0] + raw[:, 1]).reshape(-1, 1)], axis=1
        )
    elif isinstance(raw, pd.DataFrame):
        s = m * (raw.iloc[:, 0] + raw.iloc[:, 1])
        s.name = "derived feature"
        derived = pd.concat([raw, s], axis=1)
    else:
        raise TypeError(f"Invalid raw data type {type(raw)}")
    return derived


def custom_split(data, test_size, random_state, mult):
    return train_test_split(
        data * mult, test_size=test_size, random_state=random_state
    )


def feature_engineering_complex(raw, m=1):
    f = get_factor()
    if isinstance(raw, np.ndarray):
        derived = np.concatenate(
            [raw, m * f * (raw[:, 0] + raw[:, 1]).reshape(-1, 1)], axis=1
        )
    elif isinstance(raw, pd.DataFrame):
        s = m * f * (raw.iloc[:, 0] + raw.iloc[:, 1])
        s.name = "derived feature"
        derived = pd.concat([raw, s], axis=1)
    else:
        raise TypeError(f"Invalid raw data type {type(raw)}")
    return derived


def load_raw_data(how="numpy"):
    # Load the dataset
    iris_dataset = load_iris(as_frame=how == "pandas")

    if how == "numpy":
        raw = np.concatenate(
            [iris_dataset["data"], iris_dataset["target"].reshape(-1, 1)],
            axis=1,
        )
        raw[:, -1] = raw[:, -1].astype(int)
        pred = Variables(
            indexes=tuple(range(4)),
            names=iris_dataset["feature_names"],
            descriptions=("",) * len(iris_dataset["feature_names"]),
        )
        targ = Variables(
            indexes=(4,),
            names=("target",),
            descriptions=[" ".join(iris_dataset["target_names"])],
        )
    else:
        raw = pd.concat(
            ([iris_dataset["data"], iris_dataset["target"]]), axis=1
        )
        pred = Variables(
            names=iris_dataset["feature_names"],
            descriptions=("",) * len(iris_dataset["feature_names"]),
        )
        targ = Variables(
            names=("target",),
            descriptions=[" ".join(iris_dataset["target_names"])],
        )

    return raw, pred, targ
