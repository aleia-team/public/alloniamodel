import os
from uuid import uuid4

import pandas as pd
import pytest
from sklearn.metrics import accuracy_score

# Must be set before importing S3Path
os.environ["S3_PROXY_URL"] = "https://test.s3.proxy.url"
os.environ["TRACK_ID"] = str(uuid4())
os.environ["USER_TOKEN_ID"] = "x" * 16
os.environ["USER_TOKEN_SECRET"] = "x" * 32

from allonias3 import Configs, S3Path
from allonias3.minio.helpers import Minio
from minio.versioningconfig import ENABLED, VersioningConfig
from pytest_minio_mock import minio_mock, minio_mock_servers  # noqa: F401
from pytest_minio_mock.plugin import MockMinioClient

MockMinioClient.custom_remove_objects = Minio.custom_remove_objects
MockMinioClient.custom_remove_object = Minio.custom_remove_object
MockMinioClient.custom_list_objects = Minio.custom_list_objects


home_ = S3Path("output")
home_np = S3Path("output", persistent=False)


def metrics(x, y):
    return {"score": accuracy_score(x, y)}


@pytest.fixture
def complete_model(init_directories):  # noqa: ARG001
    from alloniamodel.model import AllOnIAModel
    from alloniamodel.variables import Variables

    from .functions import (
        CustomModel,
        feature_engineering_complex,
        load_raw_data,
    )

    name = "test_model"
    raw_data, predictives, targets = load_raw_data()
    model = AllOnIAModel(name)
    predictives += Variables(
        indexes=(4,), names=("derived feature",), descriptions=("",)
    )
    raw_path = home_ / "raw.npy"
    raw_path.write(raw_data)

    model.raw_set = raw_path
    model.set_variables(predictives, targets)
    model.feature_engineering_function = feature_engineering_complex
    model.postprocess_function = lambda x: x
    model.model = CustomModel(n_neighbors=1)
    model.save()
    model.close()
    return name


@pytest.fixture
def _set_always_required():
    from alloniamodel.decorator import ReadyDecorator

    always_required = ReadyDecorator._always_check

    ReadyDecorator.set_always_check(("required",))
    yield
    ReadyDecorator.always_check = always_required


@pytest.fixture(scope="session")
def set_class_attributes():
    from alloniamodel.model import AllOnIAModel

    uuid = str(uuid4())
    unique_home = home_ / uuid
    unique_home_np = home_np / uuid

    AllOnIAModel.root = S3Path() / "output" / uuid

    return AllOnIAModel.root, unique_home, unique_home_np


@pytest.fixture()
def make_dummmy_data():
    data_path = S3Path("notebooks/dataset")
    if data_path.is_dir():
        return data_path

    data = pd.DataFrame(data=[[0, 0], [0, 0]], columns=["a", "b"])
    feat1 = pd.DataFrame(data=[[0], [0]], columns=["a"])
    feat2 = pd.DataFrame(data=[[0], [0]], columns=["b"])
    feat3 = pd.DataFrame(data=[[0], [0]], columns=["0"])
    (data_path / "data.csv").write(data)
    (data_path / "feat1.csv").write(feat1)
    (data_path / "feat2.csv").write(feat2)
    (data_path / "feat3.csv").write(feat3)

    data = pd.DataFrame(data=[[1, 2], [2, 3]], columns=["a", "b"])
    feat1 = pd.DataFrame(data=[[1], [2]], columns=["a"])
    feat1.index.name = "index1"
    feat2 = pd.DataFrame(data=[[2], [3]], columns=["b"])
    feat2.index.name = "index2"
    feat3 = pd.DataFrame(data=[[2], [3]], columns=["0"])
    feat3.index.name = "index3"
    (data_path / "data.csv").write(data)
    (data_path / "feat1.csv").write(feat1)
    (data_path / "feat2.csv").write(feat2)
    (data_path / "feat3.csv").write(feat3)

    return data_path


@pytest.fixture(autouse=True)
def _minio_mock(minio_mock):  # noqa: ARG001, F811
    s3_proxy_url = (
        str(Configs.instance.S3_PROXY_URL)
        .replace("https://", "")
        .replace("http://", "")
        .rstrip("/")
    )
    client = Minio(endpoint=s3_proxy_url)
    client.make_bucket(Configs.instance.persistent_bucket_name)
    client.set_bucket_versioning(
        Configs.instance.persistent_bucket_name, VersioningConfig(ENABLED)
    )
    client.make_bucket(Configs.instance.non_persistent_bucket_name)
    yield
    client.buckets = {}


@pytest.fixture
def init_directories(set_class_attributes):
    _, unique_home, unique_home_np = set_class_attributes
    unique_home.mkdir()
    unique_home_np.mkdir()
    yield unique_home, unique_home_np
    unique_home.rmdir()
    unique_home_np.rmdir()
