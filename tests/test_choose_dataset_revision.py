import pytest
from allonias3 import S3Path
from expects import equal, expect

from alloniamodel import AllOnIAModel


@pytest.mark.parametrize(
    "path_to_give",
    (
        "data.csv",
        [
            "feat1.csv",
            "feat2.csv",
        ],
    ),
)
def test_choose_dataset_revision(make_dummmy_data, path_to_give):
    path_to_give = (
        make_dummmy_data / path_to_give
        if isinstance(path_to_give, str)
        else [make_dummmy_data / p for p in path_to_give]
    )
    for set_name in (
        "raw",
        "observations",
        "health_check",
        "health_check_observations",
    ):
        model = AllOnIAModel("iris_classification", read_only=True)
        setattr(model, f"{set_name}_set", path_to_give)
        set_raw_set = getattr(model, f"_{set_name}_set")
        expect(set_raw_set.pointer).to(equal(path_to_give))
        expect(getattr(model, f"{set_name}_set").sum().sum()).to(equal(8))
        getattr(model, f"_{set_name}_set").use_revision(
            1 if isinstance(path_to_give, S3Path) else (1, 1)
        )
        expect(getattr(model, f"{set_name}_set").sum().sum()).to(equal(0))
