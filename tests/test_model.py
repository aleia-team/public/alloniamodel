from pathlib import Path

import numpy as np
import pandas as pd
import pytest
import typeguard
from allonias3 import Configs, S3Path
from expects import (
    be,
    be_false,
    be_none,
    be_true,
    contain,
    equal,
    expect,
    have_key,
    have_len,
)
from sklearn.cluster import KMeans
from sklearn.exceptions import NotFittedError
from sklearn.neighbors import KNeighborsClassifier

from alloniamodel.errors import NotReadyError
from alloniamodel.model import AllOnIAModel
from alloniamodel.utils import DataHandler, Description, _Validator
from alloniamodel.variables import Variables

from .functions import (
    custom_split,
    feature_engineering,
    feature_engineering_buggy,
    load_raw_data,
    one_arg_func,
    three_arg_func,
    three_arg_func_with_x,
    two_arg_func,
    zero_arg_func,
)


class BadModel1:
    pass


class BadModel3:
    def fit(self, X, y):  # noqa: N803
        pass


class BadModel5:
    def fit(self, x, y):
        pass


class GoodModel:
    def fit(self, X, y):  # noqa: N803
        pass

    def predict(self, X):  # noqa: N803
        pass


class GoodModelOtherFunctions:
    def otherfit(self, X, y):  # noqa: N803, ARG002
        return y

    def otherpredict(self, X):  # noqa: N803
        return X


expected_description_data_empty = {
    "Data description": "some data info",
    "Data location": {
        "Raw (learning set)": None,
        "Derived": None,
        "Train": None,
        "Validation": None,
        "Test": None,
        "Observations (set on which to make prediction)": None,
        "Derived observations": None,
        "Predictions": {
            "train": None,
            "validation": None,
            "test": None,
            "observations": None,
        },
        "Postprocessed predictions": None,
    },
    "Data summary": {
        "Predictive variable(s)": {"x": {}},
        "Target variable(s)": {"y": {}},
    },
}

expected_description_full = {
    "Summary": "MISSING -- Details about functional context, model objectives, "
    "and various stakeholders.",
    "Status": "MISSING -- Details about model lifecycle status. Is it still in"
    " experiment or is it live ?",
    "Data": {
        "Data description": "MISSING -- Details about data used by the model,"
        " through exploration to training. Also give"
        " details about annotation methodology that was"
        " potentially used to create the training dataset.",
        "Data location": {
            "Raw (learning set)": "notebooks/dataset/data.csv",
            "Derived": "output/dataset/iris_classification/"
            "intermediary_data/derived.parquet",
            "Train": None,
            "Validation": None,
            "Test": "output/dataset/iris_classification/"
            "intermediary_data/test.parquet",
            "Observations (set on which to make prediction)": (
                "notebooks/dataset/data.csv"
            ),
            "Derived observations": "output/dataset/iris_classification/"
            "intermediary_data/"
            "derived_observations.parquet",
            "Predictions": {
                "train": None,
                "validation": None,
                "test": "output/dataset/iris_classification/"
                "intermediary_data/test_predictions.npy",
                "observations": None,
            },
            "Postprocessed predictions": None,
        },
        "Data summary": {
            "Predictive variable(s)": {"a": {"type": "<class 'int'>"}},
            "Target variable(s)": {"b": {"type": "<class 'int'>"}},
        },
    },
    "Ethic": "MISSING -- Details about ethic studies done around model data,"
    " if existing biases were addressed or not (through synthetic"
    " data for example), and the process used (through ethic comity"
    " for example).",
    "Training": "MISSING -- Details about training frequency, data scope,"
    " and targeted lifecycle (hot or cold).",
    "Explainability": "MISSING -- Details about model explicability and"
    " potential libraries that are used to addressed"
    " this topic.",
    "Tests": "MISSING -- Details about test scenarios around the model,"
    " parameters control, data preparation, and expected values.",
    "Functional validation": "MISSING -- Details about the model functionnal"
    " validation and its methodology. Did it involve"
    " functional stakeholders doing annotation &"
    " validation campaigns and how was it done ?",
    "Activations": "MISSING -- Details about rules to control the model"
    " activation & deactivation on live environment.",
    "Deployment checklist": "MISSING -- list of requirements to check before"
    " being officially able to trigger a new model"
    " deployment.",
    "Technical performances": "skip",
    "Evaluation": {
        "Validation metrics": {},
        "Test metrics": {"a": "skip", "b": "skip", "c": 4},
    },
    "Rapporteurs": [
        {"name": "coucou", "email": "coucou@coucou.com", "role": "chien"},
        {"name": "kiki", "email": "kiki@coucou.com", "role": "chat"},
    ],
    "Architecture": {"Model": "KNeighborsClassifier", "Requirements": "skip"},
}


def _test_file_revision_in_name(path_, version_, exists=True, persistent=True):
    if version_:
        path_ = (path_.parent / version_ / path_.stem).with_suffix(path_.suffix)
    expect(S3Path(path_, persistent=persistent).is_file()).to(
        be_true if exists else be_false
    )


def test_init(set_class_attributes):
    home = set_class_attributes[0]
    model = AllOnIAModel("iris_classification", read_only=True)

    expect(model.root).to(equal(home))
    expect(model.save_intermediary_data).to(be_false)
    expect(model._model_root_path).to(equal(home / "model"))
    expect(model._valid_set_names).to(
        equal(("train", "validation", "test", "observations"))
    )

    expect(model.predictive_variables.names).to(equal(("x",)))
    expect(model.target_variables.names).to(equal(("y",)))
    expect(model.validation_set_size).to(equal(0.2))
    expect(model.test_set_size).to(equal(0.2))
    expect(model.random_state).to(be_none)
    expect(model.validators).to(equal([]))
    expect(model.model).to(be_none)
    expect(model.model_class).to(be_none)
    expect(model.classes).to(be_none)
    expect(model.raw_set).to(be_none)
    expect(model.health_check_set).to(be_none)
    expect(model.health_check_observations_set).to(be_none)
    expect(model.derived_set).to(be_none)
    expect(model.train_set).to(be_none)
    expect(model.validation_set).to(be_none)
    expect(model.test_set).to(be_none)
    expect(model.observations_set).to(be_none)
    expect(model.derived_observations_set).to(be_none)

    for dataset in model._valid_set_names:
        expect(model.get_predictions(dataset)).to(be_none)
    expect(model.postprocessed_predictions_set).to(be_none)

    expect(model.feature_engineering_function(0)).to(equal(0))
    expect(model.compute_metrics_function(0, 0)).to(equal({}))
    expect(model.postprocess_function(0)).to(equal(0))

    # TODO : test requirements properly (now tested locally 'by hand' by
    #  @pcotte and it worked)

    expect(model.feature_engineering_execs.content).to(equal([]))
    expect(model.train_execs.content).to(equal([]))
    expect(model.validate_execs.content).to(equal([]))
    expect(model.test_execs.content).to(equal([]))
    expect(model.predict_execs.content).to(equal([]))
    expect(model.postprocess_execs.content).to(equal([]))

    expect(model.feature_engineering_summary.empty).to(be_true)
    expect(model.trainings_summary.empty).to(be_true)
    expect(model.validations_summary.empty).to(be_true)
    expect(model.tests_summary.empty).to(be_true)
    expect(model.predicts_summary.empty).to(be_true)
    expect(model.postprocesses_summary.empty).to(be_true)


def test_give_basic_input():  # noqa: PLR0915
    raw_data, predictives, targets = load_raw_data()

    model = AllOnIAModel("iris_classification", read_only=True)
    model.update_model_description(
        {"Summary": "some summary", "chien": "chat", "Data": "some data info"}
    )
    expect("Summary" in model.description).to(be_true)
    expect("Data" in model.description).to(be_true)
    expect("chien" in model.description).to(be_false)
    expect(model.description["Summary"]).to(equal("some summary"))
    for key in expected_description_data_empty:
        expect(model.description["Data"]).to(have_key(key))
        if isinstance(expected_description_data_empty[key], dict):
            for subkey in expected_description_data_empty[key]:
                expect(model.description["Data"][key]).to(have_key(subkey))
                if isinstance(
                    expected_description_data_empty[key][subkey], dict
                ):
                    for subsubkey in expected_description_data_empty[key][
                        subkey
                    ]:
                        expect(model.description["Data"][key][subkey]).to(
                            have_key(subsubkey)
                        )
                        expect(
                            model.description["Data"][key][subkey][subsubkey]
                        ).to(
                            equal(
                                expected_description_data_empty[key][subkey][
                                    subsubkey
                                ]
                            )
                        )
                else:
                    expect(model.description["Data"][key][subkey]).to(
                        equal(expected_description_data_empty[key][subkey])
                    )
        else:
            expect(model.description["Data"][key]).to(
                equal(expected_description_data_empty[key])
            )

    for entry in Description.VALID_USER_ENTRIES:
        if entry in ("Summary", "Data"):
            continue
        expect(model.description[entry].startswith("MISSING -- ")).to(be_true)
    expect(model.description["Architecture"]["Model"]).to(be_none)
    expect(len(model.description["Architecture"]["Requirements"]) > 0).to(
        be_true
    )
    pd.testing.assert_frame_equal(
        model.description["Technical performances"]["Durations"],
        pd.DataFrame(
            columns=["Duration (s)"],
            index=[
                "Feature engineering",
                "Train-validation-test split",
                "Training",
                "Validation",
                "Test",
                "Prediction",
                "Postprocess",
            ],
            data="nan+/-nan",
        ),
    )
    pd.testing.assert_frame_equal(
        model.description["Technical performances"]["Learnings"],
        model.learnings_summary,
    )
    pd.testing.assert_frame_equal(
        model.description["Technical performances"]["Predicts"],
        model.predicts_summary,
    )
    expect(model.description["Evaluation"]).to(
        equal({"Validation metrics": {}, "Test metrics": {}})
    )

    model.set_variables(predictives.names, None)
    expect(model.predictive_variables).to(
        equal(Variables(names=predictives.names))
    )
    expect(model.target_variables).to(be(None))

    model.set_variables(predictives, targets.names)
    expect(model.target_variables).to(equal(Variables(names=targets.names)))

    model.set_variables(
        np.array(predictives.names), pd.Index(list(targets.names))
    )
    expect(model.predictive_variables).to(
        equal(Variables(names=predictives.names))
    )
    expect(model.target_variables).to(equal(Variables(names=targets.names)))

    model.set_variables(
        pd.DataFrame(
            columns=["names", "descriptions"],
            data=[["chien", "an animal"], ["rose", "a flower"]],
        ),
        pd.Series(
            index=["indexes", "descriptions"],
            data=[[0, 1], ["an animal", "a flower"]],
        ),
    )
    expect(model.predictive_variables).to(
        equal(
            Variables(
                names=("chien", "rose"), descriptions=("an animal", "a flower")
            )
        )
    )
    expect(model.target_variables).to(
        equal(Variables(indexes=(0, 1), descriptions=("an animal", "a flower")))
    )

    model.set_variables(
        pd.DataFrame(
            index=["names", "descriptions"],
            data=[["chien", "rose"], ["an animal", "a flower"]],
        ),
        None,
    )
    expect(model.predictive_variables).to(
        equal(
            Variables(
                names=("chien", "rose"), descriptions=("an animal", "a flower")
            )
        )
    )

    model.set_variables(predictives, targets)
    expect(model.predictive_variables).to(equal(predictives))
    expect(model.target_variables).to(equal(targets))

    model.set_set_sizes(0.4, 0.3)
    expect(model.validation_set_size).to(equal(0.4))
    expect(model.test_set_size).to(equal(0.3))
    with pytest.raises(
        ValueError, match="set size must be between 0 and 1"
    ) as error:
        model.set_set_sizes(1.1, 0.3)
    expect(str(error.value)).to(
        equal("Validation set size must be between 0 and 1")
    )
    with pytest.raises(
        ValueError, match="set size must be between 0 and 1"
    ) as error:
        model.set_set_sizes(0.3, 1.1)
    expect(str(error.value)).to(equal("Test set size must be between 0 and 1"))
    with pytest.raises(ValueError, match="must be inferior to 1") as error:
        model.set_set_sizes(0.5, 0.5)
    expect(str(error.value)).to(
        equal(
            "The sum of the test and validation sizes must be " "inferior to 1"
        )
    )
    model.random_state = 1
    expect(model.random_state).to(equal(1))

    model.add_validator("toto", "email@email.com", "role")
    expect(model.validators).to(
        equal([_Validator(name="toto", email="email@email.com", role="role")])
    )

    with pytest.raises(AttributeError) as error:
        model.model = BadModel1()
    expect(
        str(error.value).startswith(
            "The model object should implement the 'fit' method"
        )
    ).to(be_true)
    with pytest.raises(AttributeError) as error:
        model.model = BadModel3()
    expect(
        str(error.value).startswith(
            "The model object should implement the 'predict' method"
        )
    ).to(be_true)
    with pytest.raises(AttributeError) as error:
        model.model = BadModel5()
    expect(
        str(error.value).startswith(
            "The model object should implement the 'predict' method"
        )
    ).to(be_true)
    model.model = KNeighborsClassifier(n_neighbors=1)
    expect(model.description["Architecture"]["Model"]).to(
        equal("KNeighborsClassifier")
    )
    expect(model.description["Architecture"]["Seldon Implementation"]).to(
        equal("SKLEARN_SERVER")
    )
    expect(isinstance(model.model, KNeighborsClassifier)).to(be_true)
    model.model = GoodModel()
    model.model_class = GoodModel
    model.model_class = KNeighborsClassifier
    expect(model.model_class).to(equal(KNeighborsClassifier))
    model.model = GoodModel()
    model.model_class = KNeighborsClassifier
    model.model = KNeighborsClassifier(n_neighbors=1)
    expect(isinstance(model.model, KNeighborsClassifier)).to(be_true)

    with pytest.raises(typeguard.TypeCheckError) as error:
        model.raw_set = [0, 1]
    expect(str(error.value)).to(contain(" did not match any element"))


def test_update_description_and_summaries(make_dummmy_data, init_directories):  # noqa: PLR0912,PLR0915,C901
    uuid = init_directories[0].name
    model = AllOnIAModel(
        "iris_classification", read_only=False, override_lock=True
    )
    path = make_dummmy_data / "data.csv"
    model.raw_set = path
    model.observations_set = path
    model.set_variables(("a",), ("b",))
    model.feature_engineering_function = feature_engineering
    model.train_val_test_split_function = custom_split
    model.set_set_sizes(0, 0.2)
    model.compute_metrics_function = three_arg_func
    model.model = KNeighborsClassifier(n_neighbors=1)
    model.add_validators(
        (
            ("coucou", "coucou@coucou.com", "chien"),
            ("kiki", "kiki@coucou.com", "chat"),
        )
    )
    model.save_intermediary_data = (
        "raw",
        "derived",
        "test",
        "test_predictions",
        "derived_observations",
    )
    model.learn(
        reshape_x=(-1, 1),
        reshape_y=(-1, 1),
        feature_engineering_kwargs={"m": 100},
        train_val_test_split_kwargs={"mult": 100},
        metrics_kwargs={"z": 2},
    )
    model.apply(reshape_x=(-1, 1), feature_engineering_kwargs={"m": 100})
    model.close()
    description = model.description

    for key in expected_description_full:
        expect(description).to(have_key(key))
        if isinstance(expected_description_full[key], dict):
            for subkey in expected_description_full[key]:
                expect(description[key]).to(have_key(subkey))
                if isinstance(expected_description_full[key][subkey], dict):
                    for subsubkey, descri_item in expected_description_full[
                        key
                    ][subkey].items():
                        if isinstance(
                            descri_item, str
                        ) and descri_item.startswith("output/"):
                            descri_item = str(  # noqa: PLW2901
                                Path(
                                    "output", uuid, *descri_item.split("/")[1:]
                                )
                            )
                        expect(description[key][subkey]).to(have_key(subsubkey))
                        if isinstance(descri_item, dict):
                            for (
                                subsubsubkey,
                                descri_item_2,
                            ) in expected_description_full[key][subkey][
                                subsubkey
                            ].items():
                                if isinstance(
                                    descri_item_2, str
                                ) and descri_item_2.startswith("output/"):
                                    descri_item_2 = str(  # noqa: PLW2901
                                        Path(
                                            "output",
                                            uuid,
                                            *descri_item_2.split("/")[1:],
                                        )
                                    )
                                expect(description[key][subkey][subsubkey]).to(
                                    have_key(subsubsubkey)
                                )
                                if descri_item_2 == "skip":
                                    expect(
                                        description[key][subkey][subsubkey]
                                    ).to(have_key(subsubsubkey))
                                    continue

                                expect(
                                    description[key][subkey][subsubkey][
                                        subsubsubkey
                                    ]
                                ).to(equal(descri_item_2))
                        elif descri_item == "skip":
                            expect(description[key][subkey]).to(
                                have_key(subsubkey)
                            )
                            continue
                        else:
                            expect(description[key][subkey][subsubkey]).to(
                                equal(descri_item)
                            )
                elif expected_description_full[key][subkey] == "skip":
                    expect(description[key]).to(have_key(subkey))
                    continue
                else:
                    expect(description[key][subkey]).to(
                        equal(expected_description_full[key][subkey])
                    )
        elif expected_description_full[key] == "skip":
            continue
        else:
            expect(description[key]).to(equal(expected_description_full[key]))

    expect(model.learnings_summary["dataset_revision"].iloc[0]).to(
        equal({"raw_set": ("notebooks/dataset/data.csv", 2)})
    )
    expect(model.feature_engineering_summary["dataset_revision"].iloc[0]).to(
        equal({"raw_set": ("notebooks/dataset/data.csv", 2)})
    )
    expect(model.feature_engineering_summary["dataset_revision"].iloc[1]).to(
        equal(
            {
                "observations_set": (
                    "notebooks/dataset/data.csv",
                    2,
                )
            }
        )
    )
    expect(model.splits_summary["dataset_revision"].iloc[0]).to(
        equal(
            {
                "derived_set": (
                    f"output/{uuid}/dataset/iris_classification/"
                    "intermediary_data/derived.parquet",
                    None,
                )
            }
        )
    )
    expect(model.trainings_summary["dataset_revision"].iloc[0]).to(
        equal({"train_set": (None, None)})
    )
    expect(model.tests_summary["dataset_revision"].iloc[0]).to(
        equal(
            {
                "test_set": (
                    f"output/{uuid}/dataset/iris_classification/"
                    "intermediary_data/test.parquet",
                    None,
                )
            }
        )
    )
    expect(model.predicts_summary["dataset_revision"].iloc[0]).to(
        equal(
            {
                "derived_observations_set": (
                    f"output/{uuid}/dataset/iris_classification/"
                    "intermediary_data/derived_observations.parquet",
                    None,
                )
            }
        )
    )
    expect(model.applies_summary["dataset_revision"].iloc[0]).to(
        equal(
            {
                "observations_set": (
                    "notebooks/dataset/data.csv",
                    2,
                )
            }
        )
    )


@pytest.mark.parametrize(
    "path_to_give",
    (
        "data.csv",
        [
            "feat1.csv",
            "feat2.csv",
        ],
    ),
)
def test_give_data_input_as_path(make_dummmy_data, path_to_give):
    path_to_give = (
        make_dummmy_data / path_to_give
        if isinstance(path_to_give, str)
        else [make_dummmy_data / p for p in path_to_give]
    )
    for set_name in (
        "raw",
        "observations",
        "health_check",
        "health_check_observations",
    ):
        model = AllOnIAModel("iris_classification", read_only=True)
        setattr(model, f"{set_name}_set", path_to_give)
        set_raw_set = getattr(model, f"_{set_name}_set")
        set_raw_set.concatenate_kwargs = {"axis": 1}
        expect(isinstance(set_raw_set, DataHandler)).to(be_true)
        expect(set_raw_set.pointer).to(equal(path_to_give))
        expect(set_raw_set._method).to(
            equal(
                "from_file"
                if isinstance(path_to_give, S3Path)
                else "from_files"
            )
        )
        expect(set_raw_set._data).to(be_none)
        pd.testing.assert_frame_equal(
            getattr(model, f"{set_name}_set"),
            (make_dummmy_data / "data.csv").read(index_col=0, revision=2),
            check_names=False,
        )


@pytest.mark.parametrize("how", ("numpy", "pandas"))
def test_give_data_input_as_raw(how):
    raw_data, predictives, targets = load_raw_data(how)
    model = AllOnIAModel("iris_classification", read_only=True)
    for set_name in (
        "raw",
        "observations",
        "health_check",
        "health_check_observations",
    ):
        setattr(model, f"{set_name}_set", raw_data)
        set_raw_set = getattr(model, f"_{set_name}_set")
        expect(isinstance(set_raw_set, DataHandler)).to(be_true)
        expect(set_raw_set.pointer).to(be_none)
        expect(set_raw_set._method).to(equal(""))
        if how == "numpy":
            np.testing.assert_equal(set_raw_set._data, raw_data)
            np.testing.assert_equal(getattr(model, f"{set_name}_set"), raw_data)
        else:
            pd.testing.assert_frame_equal(set_raw_set._data, raw_data)
            pd.testing.assert_frame_equal(
                getattr(model, f"{set_name}_set"), raw_data
            )
        expect(
            S3Path(
                model.intermediary_save_path / f"{set_name}.alloniamodel"
            ).is_file()
        ).to(be_false)


@pytest.mark.parametrize(
    ("how", "sets"), (("numpy", True), ("pandas", ("raw", "observations")))
)
@pytest.mark.usefixtures("init_directories")
def test_give_data_input_as_raw_save(how, sets):
    raw_data, predictives, targets = load_raw_data(how)
    model = AllOnIAModel(
        "iris_classification", read_only=False, override_lock=True
    )
    model.save_intermediary_data = sets
    for set_name in (
        "raw",
        "observations",
        "health_check",
        "health_check_observations",
    ):
        if how == "numpy":
            path = model.intermediary_save_path / f"{set_name}.npy"
        else:
            path = model.intermediary_save_path / f"{set_name}.parquet"
        setattr(model, f"{set_name}_set", raw_data)
        if how == "numpy":
            if sets is True or set_name in sets:
                expect(S3Path(path).is_file()).to(be_true)
                np.testing.assert_equal(
                    S3Path(path).read(),
                    raw_data,
                )
                S3Path(path).read()
            else:
                expect(S3Path(path).is_file()).to(be_false)
        elif sets is True or set_name in sets:
            expect(S3Path(path).is_file()).to(be_true)
            pd.testing.assert_frame_equal(
                S3Path(path).read(),
                raw_data,
            )
            S3Path(path).rm()
        else:
            expect(S3Path(path).is_file()).to(be_false)
    model._release_lock(None, None)


def test_give_functions_input():
    model = AllOnIAModel("iris_classification", read_only=True)

    with pytest.raises(TypeError) as error:
        model.feature_engineering_function = zero_arg_func()
    expect(str(error.value)).to(contain("callable"))
    with pytest.raises(TypeError) as error:
        model.train_val_test_split_function = zero_arg_func()
    expect(str(error.value)).to(contain("callable"))
    with pytest.raises(TypeError) as error:
        model.compute_metrics_function = zero_arg_func()
    expect(str(error.value)).to(contain("callable"))
    with pytest.raises(TypeError) as error:
        model.postprocess_function = zero_arg_func()
    expect(str(error.value)).to(contain("callable"))

    with pytest.raises(ValueError, match="function must accept at least 1"):
        model.feature_engineering_function = zero_arg_func
    with pytest.raises(ValueError, match="function must accept at least 3"):
        model.train_val_test_split_function = zero_arg_func
    with pytest.raises(ValueError, match="function must accept at least 1"):
        model.compute_metrics_function = zero_arg_func
    with pytest.raises(ValueError, match="function must accept at least 1"):
        model.postprocess_function = zero_arg_func

    model.feature_engineering_function = lambda x: x
    expect(model.feature_engineering_function(2)).to(equal(2))
    model.feature_engineering_function = one_arg_func
    model.train_val_test_split_function = three_arg_func
    model.compute_metrics_function = two_arg_func
    model.postprocess_function = one_arg_func

    expect(model.feature_engineering_function(2)).to(equal(4))
    expect(model.train_val_test_split_function(1, 2, 3)).to(
        equal({"a": 2, "b": 4, "c": 6})
    )
    expect(model.compute_metrics_function(1, 2)).to(equal((2, 4)))
    expect(model.postprocess_function(2)).to(equal(4))


@pytest.mark.parametrize("how", ("numpy", "pandas"))
def test_feat_eng_raw(how):
    model = AllOnIAModel("iris_classification", read_only=True)
    raw_data, predictives, targets = load_raw_data(how)
    with pytest.raises(NotReadyError):
        model.feature_engineering("raw")

    model.raw_set = raw_data

    with pytest.raises(
        ValueError,
        match="Invalid value 'coucou' for raw_or_observed",
    ):
        model.feature_engineering("coucou")
    derived = model.feature_engineering("raw")
    model.feature_engineering_function = feature_engineering

    if how == "numpy":
        np.testing.assert_equal(raw_data, model.derived_set)
        np.testing.assert_equal(raw_data, derived)

        derived = model.feature_engineering("raw", m=2)
        np.testing.assert_equal(
            2 * (raw_data[:, 0] + raw_data[:, 1]), model.derived_set[:, -1]
        )
        np.testing.assert_equal(
            2 * (raw_data[:, 0] + raw_data[:, 1]), derived[:, -1]
        )
        expect(model.feature_engineering_execs.content[-1].parameters).to(
            equal({"m": 2})
        )
    else:
        pd.testing.assert_frame_equal(raw_data, model.derived_set)
        pd.testing.assert_frame_equal(raw_data, derived)

        derived = model.feature_engineering(raw_or_observed="raw", m=2)
        pd.testing.assert_series_equal(
            2 * (raw_data.iloc[:, 0] + raw_data.iloc[:, 1]),
            model.derived_set.iloc[:, -1],
            check_names=False,
        )
        pd.testing.assert_series_equal(
            2 * (raw_data.iloc[:, 0] + raw_data.iloc[:, 1]),
            derived.iloc[:, -1],
            check_names=False,
        )
        expect(model.feature_engineering_execs.content[-1].parameters).to(
            equal({"raw_or_observed": "raw", "m": 2})
        )


@pytest.mark.parametrize("how", ("numpy", "pandas"))
def test_split(how):
    model = AllOnIAModel("iris_classification", read_only=True)

    raw_data, predictives, targets = load_raw_data(how)
    model.raw_set = raw_data
    model.set_variables(predictives, targets)
    model.feature_engineering_function = feature_engineering

    with pytest.raises(NotReadyError):
        model.train_val_test_split()

    derived = model.feature_engineering("raw")
    train, validation, test = model.train_val_test_split()

    if how == "numpy":
        expect(train.shape[0]).to(equal(int(0.6 * derived.shape[0])))
        expect(train.shape[1]).to(equal(derived.shape[1]))
        expect(test.shape[0]).to(equal(int(0.2 * derived.shape[0])))
        expect(test.shape[1]).to(equal(derived.shape[1]))
        expect(validation.shape[0]).to(equal(int(0.2 * derived.shape[0])))
        expect(validation.shape[1]).to(equal(derived.shape[1]))

        np.testing.assert_equal(train, model.train_set)
        np.testing.assert_equal(validation, model.validation_set)
        np.testing.assert_equal(test, model.test_set)
    else:
        expect(len(train.index)).to(equal(int(0.6 * len(derived.index))))
        expect(len(train.columns)).to(equal(len(derived.columns)))
        expect(len(test.index)).to(equal(int(0.2 * len(derived.index))))
        expect(len(test.columns)).to(equal(len(derived.columns)))
        expect(len(validation.index)).to(equal(int(0.2 * len(derived.index))))
        expect(len(validation.columns)).to(equal(len(derived.columns)))

        pd.testing.assert_frame_equal(train, model.train_set)
        pd.testing.assert_frame_equal(validation, model.validation_set)
        pd.testing.assert_frame_equal(test, model.test_set)


@pytest.mark.parametrize(
    ("how", "set_sizes"),
    (
        ("numpy", (0.2, 0.2)),
        ("pandas", (0.2, 0.2)),
        ("numpy", (0, 0.2)),
        ("pandas", (0, 0.2)),
        ("numpy", (0.2, 0)),
        ("pandas", (0.2, 0)),
        ("numpy", (0, 0)),
        ("pandas", (0, 0)),
    ),
)
def test_full_pipeline(how, set_sizes):  # noqa: PLR0915
    model = AllOnIAModel("iris_classification", read_only=True)

    raw_data, predictives, targets = load_raw_data(how)
    if how == "numpy":
        predictives += Variables(
            indexes=(4,), names=("derived feature",), descriptions=("",)
        )
    else:
        predictives += Variables(names=("derived feature",), descriptions=("",))
    model.raw_set = raw_data
    model.set_variables(predictives, targets)
    model.feature_engineering_function = feature_engineering
    model.train_val_test_split_function = custom_split
    model.set_set_sizes(*set_sizes)
    model.compute_metrics_function = three_arg_func
    model.feature_engineering("raw")
    expect(model.target_variables.data).not_to(have_key("types"))
    expect(model.predictive_variables.data).not_to(have_key("types"))

    with pytest.raises(NotReadyError):
        model.train()

    model.train_val_test_split(mult=100)

    with pytest.raises(NotReadyError):
        model.train()

    model.model = KNeighborsClassifier(n_neighbors=1)

    if set_sizes[1] > 0:
        with pytest.raises(NotFittedError):
            model.test()
    if set_sizes[0] > 0:
        with pytest.raises(NotFittedError):
            model.validate()
    with pytest.raises(NotReadyError):
        model.predict()

    expect(len(model.train_execs.content)).to(equal(0))
    expect(len(model.validate_execs.content)).to(equal(0))
    expect(len(model.test_execs.content)).to(equal(0))
    expect(len(model.predict_execs.content)).to(equal(0))
    model.train()
    if how == "numpy":
        expect(model.target_variables.data["types"]).to(equal((float,)))
        expect(model.classes).to(be_none)
    else:
        expect(model.target_variables.data["types"]).to(equal((int,)))
        expect(len(model.classes)).to(equal(3))
    expect(model.predictive_variables.data["types"]).to(
        equal((float, float, float, float, float))
    )
    expect(len(model.train_execs.content)).to(equal(1))
    model.validate()
    expect(len(model.validate_execs.content)).to(equal(1))
    model.test()
    expect(len(model.test_execs.content)).to(equal(1))
    with pytest.raises(NotReadyError):
        model.predict()
    model.observations_set = raw_data
    with pytest.raises(NotReadyError):
        model.predict()
    model.feature_engineering("observed")
    model.predict()
    expect(len(model.predict_execs.content)).to(equal(1))

    model.learn(
        model_kwargs={"n_neighbors": 1},
        feature_engineering_kwargs={"m": 100},
        train_val_test_split_kwargs={"mult": 100},
    )
    expect(len(model.train_execs.content)).to(equal(2))
    expect(len(model.validate_execs.content)).to(equal(2))
    expect(len(model.test_execs.content)).to(equal(2))
    if set_sizes[-1] > 0:
        expect(model.test_execs.content[-1].results["c"]).to(equal(2))
        model.learn(
            model_kwargs={"n_neighbors": 1},
            feature_engineering_kwargs={"m": 100},
            train_val_test_split_kwargs={"mult": 100},
            metrics_kwargs={"z": 2},
        )
        expect(model.test_execs.content[-1].results["c"]).to(equal(4))

    model.apply(feature_engineering_kwargs={"m": 100})
    expect(len(model.predict_execs.content)).to(equal(2))


@pytest.mark.parametrize(("how", "set_sizes"), (("numpy", (0.2, 0.2)),))
def test_full_pipeline_x_in_metrics(how, set_sizes):
    model = AllOnIAModel("iris_classification", read_only=True)

    raw_data, predictives, targets = load_raw_data(how)
    predictives += Variables(
        indexes=(4,), names=("derived feature",), descriptions=("",)
    )
    model.raw_set = raw_data
    model.observations_set = raw_data
    model.set_variables(predictives, targets)
    model.feature_engineering_function = feature_engineering
    model.train_val_test_split_function = custom_split
    model.set_set_sizes(*set_sizes)
    model.compute_metrics_function = three_arg_func_with_x
    model.feature_engineering("raw")
    expect(model.target_variables.data).not_to(have_key("types"))
    expect(model.predictive_variables.data).not_to(have_key("types"))
    model.train_val_test_split(mult=100)
    model.model = KNeighborsClassifier(n_neighbors=1)

    model.learn(
        model_kwargs={"n_neighbors": 1},
        feature_engineering_kwargs={"m": 100},
        train_val_test_split_kwargs={"mult": 100},
    )
    if set_sizes[-1] > 0:
        np.testing.assert_equal(
            model.test_set[:, model.predictive_variables.identities] * 2,
            model.test_execs.content[-1].results["c"],
        )
        model.learn(
            model_kwargs={"n_neighbors": 1},
            feature_engineering_kwargs={"m": 100},
            train_val_test_split_kwargs={"mult": 100},
            metrics_kwargs={"x": 4},
        )
        expect(model.test_execs.content[-1].results["c"]).to(equal(8))

    model.apply(feature_engineering_kwargs={"m": 100})
    expect(len(model.predict_execs.content)).to(equal(1))


@pytest.mark.parametrize("how", ("numpy", "pandas"))
def test_no_target(how):
    model = AllOnIAModel("iris_classification", read_only=True)

    raw_data, predictives, _ = load_raw_data(how)
    model.set_variables(predictives)
    if how == "numpy":
        predictives += Variables(
            indexes=(4,), names=("derived feature",), descriptions=("",)
        )
    else:
        predictives += Variables(names=("derived feature",), descriptions=("",))
    model.raw_set = raw_data
    model.set_variables(predictives)
    model.feature_engineering_function = feature_engineering
    model.feature_engineering("raw")
    model.train_val_test_split()
    model.model = KMeans(n_clusters=3)

    expect(len(model.train_execs.content)).to(equal(0))
    expect(len(model.validate_execs.content)).to(equal(0))
    expect(len(model.test_execs.content)).to(equal(0))
    expect(len(model.predict_execs.content)).to(equal(0))
    model.train()
    expect(len(model.train_execs.content)).to(equal(1))
    model.validate()
    expect(len(model.validate_execs.content)).to(equal(1))
    model.test()
    expect(len(model.test_execs.content)).to(equal(1))
    model.observations_set = raw_data
    model.feature_engineering("observed")
    model.predict()
    expect(len(model.predict_execs.content)).to(equal(1))

    model.learn()
    expect(len(model.train_execs.content)).to(equal(2))
    expect(len(model.validate_execs.content)).to(equal(2))
    expect(len(model.test_execs.content)).to(equal(2))

    model.apply()
    expect(len(model.predict_execs.content)).to(equal(2))


@pytest.mark.parametrize("how", ("numpy", "pandas"))
@pytest.mark.usefixtures("init_directories")
def test_save_load(how, make_dummmy_data):  # noqa: PLR0915
    model = AllOnIAModel(
        f"iris_classification_{how}", read_only=False, override_lock=True
    )
    data_path = make_dummmy_data

    raw_data, predictives, targets = load_raw_data(how)
    if how == "numpy":
        predictives += Variables(
            indexes=(4,), names=("derived feature",), descriptions=("",)
        )
        raw_path = data_path / "raw.npy"
    else:
        predictives += Variables(names=("derived feature",), descriptions=("",))
        raw_path = data_path / "raw.parquet"
    raw_path.write(raw_data)

    expect(S3Path(model.binary_path).is_file()).to(be_false)
    expect(S3Path(model.binary_path).with_suffix(".joblib").is_file()).to(
        be_false
    )
    expect(model.open_by_path.is_file()).to(be_true)
    open_by = model.open_by_path.read()
    expect(len(open_by)).to(equal(1))

    open_by = next(iter(open_by.values()))
    open_by["open_by"].pop("file")
    expect(open_by).to(
        equal(
            {
                "open_by": {
                    "track": str(Configs.instance.TRACK_ID),
                    "project": "None",
                    "user": "None",
                }
            }
        )
    )
    model.save()
    expect(S3Path(model.binary_path).is_file()).to(be_true)

    model_2 = AllOnIAModel(f"iris_classification_{how}", read_only=True)

    open_by = model.open_by_history
    expect(len(open_by)).to(equal(2))
    open_by = next(iter(open_by.values()))
    open_by["open_by"].pop("file")
    expect(open_by).to(
        equal(
            {
                "open_by": {
                    "track": str(Configs.instance.TRACK_ID),
                    "project": "None",
                    "user": "None",
                }
            }
        )
    )

    model.raw_set = raw_path
    model.save()
    expect(S3Path(model.binary_path).with_suffix(".joblib").is_file()).to(
        be_false
    )

    model_3 = AllOnIAModel(f"iris_classification_{how}", read_only=True)

    open_by = model.open_by_path.read()
    expect(len(open_by)).to(equal(3))
    open_by = next(iter(open_by.values()))
    open_by["open_by"].pop("file")
    expect(open_by).to(
        equal(
            {
                "open_by": {
                    "track": str(Configs.instance.TRACK_ID),
                    "project": "None",
                    "user": "None",
                }
            }
        )
    )

    model.set_variables(predictives, targets)
    model.feature_engineering_function = feature_engineering
    model.model = KNeighborsClassifier(n_neighbors=1)
    model.learn()
    model.save()
    expect(model.list_versions(model)).to(have_len(3))
    for i, version in enumerate(model.list_versions(model)["version"]):
        _test_file_revision_in_name(
            S3Path(model.binary_path).with_suffix(".joblib"),
            version,
            False,
            i == 2,
        )
    _test_file_revision_in_name(
        model.description_path, model.list_versions(model)["version"].iloc[0]
    )
    _test_file_revision_in_name(
        S3Path(model.binary_path).with_suffix(".joblib"), None, False
    )
    _test_file_revision_in_name(model.description_path, None, False)

    model_4 = AllOnIAModel(f"iris_classification_{how}", read_only=True)

    open_by = model.open_by_path.read()
    expect(len(open_by)).to(equal(4))
    open_by = next(iter(open_by.values()))
    open_by["open_by"].pop("file")
    expect(open_by).to(
        equal(
            {
                "open_by": {
                    "track": str(Configs.instance.TRACK_ID),
                    "project": "None",
                    "user": "None",
                }
            }
        )
    )

    model_4.observations_set = raw_data
    model_4.apply()
    model.close()
    model_2.close()
    model_3.close()
    model_4.close()
    open_by = model.open_by_path.read()
    expect(len(open_by)).to(equal(4))
    for d in open_by:
        expect(len(open_by[d])).to(equal(2))
        open_by[d]["open_by"].pop("file")
        expect(open_by[d]["open_by"]).to(
            equal(
                {
                    "track": str(Configs.instance.TRACK_ID),
                    "project": "None",
                    "user": "None",
                }
            )
        )
        expect(open_by[d]).to(have_key("closed_at"))

    _ = AllOnIAModel(f"iris_classification_{how}", fast=True)


@pytest.mark.parametrize("how", ("numpy", "pandas"))
def test_health_check(how):
    model = AllOnIAModel("iris_classification", read_only=True)

    raw_data, predictives, targets = load_raw_data(how)
    if how == "numpy":
        predictives += Variables(
            indexes=(4,), names=("derived feature",), descriptions=("",)
        )
    else:
        predictives += Variables(names=("derived feature",), descriptions=("",))
    model.raw_set = raw_data
    model.observations_set = raw_data
    model.set_variables(predictives)
    model.feature_engineering_function = feature_engineering
    model.feature_engineering("raw")
    model.train_val_test_split()
    model.model = KMeans(n_clusters=3)

    expect(model._rerun_health_check).to(be_true)
    expect(model._healthy).to(be_false)
    expect(model.health_check()).to(be_false)
    model.health_check_set = raw_data
    expect(model.health_check()).to(be_true)
    expect(model._rerun_health_check).to(be_false)
    expect(model._healthy).to(be_true)
    expect(model.health_check()).to(be_true)
    model.feature_engineering_function = feature_engineering_buggy
    expect(model._rerun_health_check).to(be_true)
    expect(model._healthy).to(be_true)
    expect(model.health_check()).to(be_false)
    expect(model._rerun_health_check).to(be_true)
    expect(model._healthy).to(be_false)
    expect(model.health_check()).to(be_false)
    model.feature_engineering_function = feature_engineering
    expect(model._healthy).to(be_false)
    expect(model.health_check()).to(be_true)
    expect(model.health_check()).to(be_true)
    expect(model._rerun_health_check).to(be_false)
    expect(model._healthy).to(be_true)


@pytest.mark.usefixtures("init_directories")
@pytest.mark.second
def test_delete():
    model = AllOnIAModel("iris_classification", read_only=False)
    model.model = GoodModel()
    model.save()
    model.save()
    model.save()
    model.close()
    AllOnIAModel.update_description("iris_classification", {"Summary": "1"}, 1)
    AllOnIAModel.update_description("iris_classification", {"Summary": "2"}, 2)
    AllOnIAModel.update_description("iris_classification", {"Summary": "3"}, 3)

    versions = model.list_versions(model)

    expect(versions).to(have_len(3))
    for i, version in enumerate(versions["version"]):
        _test_file_revision_in_name(
            model.binary_path.with_suffix(".joblib"), version, False
        )
        _test_file_revision_in_name(model.description_path, version)
        revisioned_description = model._make_description_path(
            model.name,
            model._make_paths(model.name, ("description",))[0],
        ).read(i + 1, False, False)
        expect(revisioned_description["Summary"]).to(equal(str(i + 1)))
        versioned_description = model._make_description_path(
            model.name,
            model._make_paths(model.name, ("description",))[0],
        ).read(version, False, False)
        expect(versioned_description["Summary"]).to(equal(str(i + 1)))

    latest_description = S3Path(model.description_path, persistent=False).read()
    expect(latest_description["Summary"]).to(equal("3"))

    AllOnIAModel.delete("iris_classification", revision=2)

    for i, (expected, exists) in enumerate(
        (("1", True), (None, False), ("3", True)),
    ):
        version = versions["version"].iloc[i]
        _test_file_revision_in_name(
            model.binary_path.with_suffix(".joblib"), version, False
        )
        _test_file_revision_in_name(model.description_path, version, exists)
        if exists:
            revisioned_description = model._make_description_path(
                model.name,
                model._make_paths(model.name, ("description",))[0],
            ).read(version, False, False)
            expect(revisioned_description["Summary"]).to(equal(expected))

    latest_description = S3Path(model.description_path, persistent=False).read()
    expect(latest_description["Summary"]).to(equal("3"))

    AllOnIAModel.delete("iris_classification", revision=2)

    for i, (expected, exists) in enumerate(
        (("1", True), (None, False), (None, False))
    ):
        version = versions["version"].iloc[i]
        _test_file_revision_in_name(
            model.binary_path.with_suffix(".joblib"), version, False
        )
        _test_file_revision_in_name(model.description_path, version, exists)
        if exists:
            revisioned_description = model._make_description_path(
                model.name,
                model._make_paths(model.name, ("description",))[0],
            ).read(version, False, False)
            expect(revisioned_description["Summary"]).to(equal(expected))

    latest_description = S3Path(model.description_path, persistent=False).read()
    expect(latest_description["Summary"]).to(equal("1"))


def test_custom_function_names():
    model = AllOnIAModel("iris_classification", read_only=True)
    with pytest.raises(AttributeError):
        model.model = GoodModelOtherFunctions
    model.fit_function_name = "otherfit"
    model.predict_function_name = "otherpredict"
    model.model = GoodModelOtherFunctions
