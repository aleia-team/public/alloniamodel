# ruff: noqa: F401, F811

import pytest

from alloniamodel.model import AllOnIAModel


def test_unpickle_complex_function(complete_model):
    with pytest.raises(
        ModuleNotFoundError, match="No module named 'functions'"
    ):
        from functions import (
            feature_engineering_complex,
            load_raw_data,
        )
    with pytest.raises(
        ImportError, match="No module named 'tests.unpickle.functions"
    ):
        from .functions import (
            feature_engineering_complex,
            load_raw_data,
        )

    model = AllOnIAModel(complete_model, override_lock=True)
    model.learn()
