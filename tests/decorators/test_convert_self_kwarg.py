from expects import equal, expect

from alloniamodel.decorator import convert_self_kwarg


class Dummy:
    def __init__(self, a):
        self.a = a

    @convert_self_kwarg
    def method(self, b, **kwargs):
        return b + kwargs["dummy"].a


def test_convert_self_kwarg():
    dummy = Dummy(1)
    expect(dummy.method(2, dummy="self", chien=2)).to(equal(3))
