import pytest

from alloniamodel.decorator import _limit_arguments


class DummyClass:
    function = None

    @_limit_arguments(number_of_args=1)
    def set_func1(self, function):
        self.function = function

    @_limit_arguments(min_args=1)
    def set_func2(self, function):
        self.function = function

    @_limit_arguments(max_args=1)
    def set_func3(self, function):
        self.function = function

    @_limit_arguments(min_args=2, max_args=3)
    def set_func4(self, function):
        self.function = function


def test_limit_arguments():
    dummy = DummyClass()

    with pytest.raises(
        ValueError,
        match="This function must accept 1 and only 1 argument. 0",
    ):
        dummy.set_func1(lambda: None)

    dummy.set_func1(lambda x: None)  # noqa: ARG005

    with pytest.raises(
        ValueError,
        match="This function must accept 1 and only 1 argument. 2",
    ):
        dummy.set_func1(lambda x, y: None)  # noqa: ARG005

    with pytest.raises(
        ValueError,
        match="This function must accept at least 1 arguments. 0",
    ):
        dummy.set_func2(lambda: None)

    dummy.set_func2(lambda x: None)  # noqa: ARG005
    dummy.set_func2(lambda x, y: None)  # noqa: ARG005

    dummy.set_func3(lambda: None)
    dummy.set_func3(lambda x: None)  # noqa: ARG005

    with pytest.raises(
        ValueError, match="This function must accept at most 1 arguments. 2"
    ):
        dummy.set_func3(lambda x, y: None)  # noqa: ARG005

    with pytest.raises(
        ValueError, match="This function must accept at least 2 arguments. 0"
    ):
        dummy.set_func4(lambda: None)

    with pytest.raises(
        ValueError, match="This function must accept at least 2 arguments. 1"
    ):
        dummy.set_func4(lambda x: None)  # noqa: ARG005

    with pytest.raises(
        ValueError, match="This function must accept at most 3 arguments. 4"
    ):
        dummy.set_func4(lambda x, y, z, a: None)  # noqa: ARG005

    dummy.set_func4(lambda x, y: None)  # noqa: ARG005
