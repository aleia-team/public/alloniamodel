import pytest

from alloniamodel.decorator import ReadyDecorator
from alloniamodel.errors import NotReadyError


class DummyClass:
    def __init__(self):
        self.required = None
        self.required_2 = None
        self.not_required = None
        self.required_dict = None

    @ReadyDecorator.ready(needed_attributes=())
    def func1(self):
        pass

    @ReadyDecorator.ready(needed_attributes=("required_2",))
    def func2(self):
        pass

    @ReadyDecorator.ready(needed_attributes=("required_2", "required_3"))
    def func3(self):
        pass

    @ReadyDecorator.ready(needed_attributes=("not_existing",))
    def func4(self):
        pass

    @ReadyDecorator.ready(needed_attributes=(("required_dict", "key"),))
    def func5(self):
        pass

    @ReadyDecorator.ready(
        needed_attributes=("(required_dict, key) | required_2",)
    )
    def func6(self):
        pass


def test_ready(_set_always_required):  # noqa: PLR0915
    dummy = DummyClass()
    with pytest.raises(NotReadyError) as e:
        dummy.func1()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func2()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func3()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func6()
    print(f"Raised:\n{e.value}")

    dummy.required = 0

    dummy.func1()
    with pytest.raises(NotReadyError) as e:
        dummy.func2()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func3()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func6()
    print(f"Raised:\n{e.value}")

    dummy.required_2 = 1

    dummy.func1()
    dummy.func2()
    with pytest.raises(NotReadyError) as e:
        dummy.func3()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    dummy.func6()

    dummy.required_3 = 2

    dummy.func1()
    dummy.func2()
    dummy.func3()
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    dummy.func6()

    dummy.required_dict = {}

    dummy.func1()
    dummy.func2()
    dummy.func3()
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    dummy.func6()

    dummy.required_dict = {"key": None}

    dummy.func1()
    dummy.func2()
    dummy.func3()
    with pytest.raises(NotReadyError) as e:
        dummy.func5()
    print(f"Raised:\n{e.value}")
    dummy.func6()

    dummy.required_dict = {"key": 0}

    dummy.func1()
    dummy.func2()
    dummy.func3()
    dummy.func5()
    dummy.func6()

    dummy.required_2 = None

    dummy.func1()
    with pytest.raises(NotReadyError) as e:
        dummy.func2()
    print(f"Raised:\n{e.value}")
    with pytest.raises(NotReadyError) as e:
        dummy.func3()
    print(f"Raised:\n{e.value}")
    dummy.func5()
    dummy.func6()
