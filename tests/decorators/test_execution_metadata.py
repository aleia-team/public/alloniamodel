import datetime
from time import sleep

import pandas as pd
import pytest
from expects import be_above_or_equal, be_none, be_true, equal, expect

from alloniamodel.decorator import _record_execution
from alloniamodel.utils import ExecutionList


class DummyClass:
    def __init__(self):
        self.store = ExecutionList(self)
        self._revision = 0
        self.name = "dummy"

    @property
    def revision(self):
        return self._revision

    @_record_execution(
        to_attribute="store",
        record_results=True,
    )
    def execute1(self, a, b, c=0, d=0):  # noqa: ARG002
        sleep(0.2)
        return {"results": a + b, "model": "chat", "dataset_revision": {}}

    @_record_execution(
        to_attribute="store",
        record_results=False,
    )
    def execute2(self, a, b, c=0, d=0):  # noqa: ARG002
        sleep(0.6)
        return {"results": c + d, "model": "chien", "dataset_revision": {}}

    @_record_execution(
        to_attribute="store",
        record_results=True,
    )
    def execute3(self, a, b, c=0, d=0):  # noqa: ARG002
        raise ValueError("error")

    @_record_execution(
        to_attribute="store", record_results=True, raise_error=False
    )
    def execute4(self, a, b, c=0, d=0):  # noqa: ARG002
        raise ValueError("error")

    @classmethod
    def list_versions(cls, _):
        return pd.DataFrame([["v0", 0]], columns=["version", "revision"])


def test_record_execution():
    dummy = DummyClass()

    dummy.execute1(0, 1, 2, 3)
    expect(len(dummy.store.content)).to(equal(1))
    e = dummy.store.content[0]
    expect(type(e.date)).to(equal(datetime.datetime))
    expect(e.duration).to(be_above_or_equal(0.2))
    expect(e.parameters).to(equal({}))
    expect(e.results).to(equal(1))
    expect(e.model).to(equal("chat"))

    dummy.execute2(0, 1, c=2, d=3)
    expect(len(dummy.store.content)).to(equal(2))
    e = dummy.store.content[1]
    expect(type(e.date)).to(equal(datetime.datetime))
    expect(e.duration).to(be_above_or_equal(0.6))
    expect(e.parameters).to(equal({"c": 2, "d": 3}))
    expect(e.results).to(be_none)
    expect(e.model).to(equal("chien"))

    sumary = dummy.store.summary
    expect(type(sumary)).to(equal(pd.DataFrame))
    expect(list(sumary.columns)).to(
        equal(
            [
                "dataset_revision",
                "model",
                "duration",
                "parameters",
                "version_id",
                "results",
                "revision",
            ]
        )
    )
    expect(sumary.index.name).to(equal("date"))
    expect(len(sumary.index)).to(equal(2))

    with pytest.raises(ValueError, match="error"):
        dummy.execute3(0, 1, 2, 3)

    res = dummy.execute4(0, 1, 2, 3)
    expect(isinstance(res, ValueError)).to(be_true)
    expect(dummy.store.content[-1].results).to(equal("ValueError: error"))
