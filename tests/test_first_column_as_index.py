import pytest
from allonias3 import S3Path
from expects import expect, have_len

from alloniamodel import AllOnIAModel


@pytest.mark.parametrize(
    "path_to_give",
    (
        "data.csv",
        [
            "feat1.csv",
            "feat2.csv",
        ],
    ),
)
def test_first_column_as_index(make_dummmy_data, path_to_give):
    path_to_give = (
        make_dummmy_data / path_to_give
        if isinstance(path_to_give, str)
        else [make_dummmy_data / p for p in path_to_give]
    )
    model = AllOnIAModel("iris_classification", read_only=True)
    model.raw_set = path_to_give
    raw_set = model.raw_set
    expect(raw_set.columns).to(have_len(2))
    model._raw_set.load_kwargs = {"index_col": None}
    model.observations_set = path_to_give
    model._observations_set.load_kwargs = {"index_col": None}
    observations_set = model.observations_set
    expect(observations_set.columns).to(
        have_len(3 if isinstance(path_to_give, S3Path) else 4)
    )
