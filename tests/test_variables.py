import pytest
from expects import equal, expect

from alloniamodel import Variables


def test_variables():
    variables = Variables(
        indexes=[0, 1],
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )
    expect(variables.indexes).to(equal([0, 1]))
    expect(variables.identities).to(equal([0, 1]))
    expect(variables.names).to(equal(("chien", "chat")))
    expect(variables.types).to(equal((int, float)))
    expect(variables.descriptions).to(equal(["a", "b"]))

    expect(variables[0]).to(equal((0, "chien", "a", int)))
    expect(variables[1]).to(equal((1, "chat", "b", float)))

    for index_, _ in enumerate(variables):
        if index_ == 0:
            expect(variables[0]).to(equal((0, "chien", "a", int)))
        else:
            expect(variables[1]).to(equal((1, "chat", "b", float)))

    with pytest.raises(TypeError):
        Variables(
            indexes=[0, 0.1],
            descriptions=["a", "b"],
            names=("chien", "chat"),
            types=(int, float),
            useless_stuff=(),
        )

    with pytest.raises(TypeError):
        Variables(
            indexes=("0", "1"),
            descriptions=["a", "b"],
            names=("chien", "chat"),
            types=(int, float),
            useless_stuff=(),
        )

    with pytest.raises(ValueError, match="Variables was provided with"):
        Variables(
            indexes=[0],
            descriptions=["a", "b"],
            names=("chien", "chat"),
            types=(int, float),
            useless_stuff=(),
        )

    variables = Variables(
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )
    expect(variables.indexes).to(equal(()))
    expect(variables.names).to(equal(("chien", "chat")))
    expect(variables.identities).to(equal(["chien", "chat"]))
    expect(variables.types).to(equal((int, float)))
    expect(variables.descriptions).to(equal(["a", "b"]))

    expect(variables[0]).to(equal(("chien", "a", int)))
    expect(variables[1]).to(equal(("chat", "b", float)))

    for index_, _ in enumerate(variables):
        if index_ == 0:
            expect(variables[0]).to(equal(("chien", "a", int)))
        else:
            expect(variables[1]).to(equal(("chat", "b", float)))

    variables = Variables(
        descriptions=["a", "b"],
        indexes=(0, 1),
        types=(int, float),
        useless_stuff=(),
    )
    expect(variables.names).to(equal(()))
    expect(variables.indexes).to(equal((0, 1)))
    expect(variables.identities).to(equal([0, 1]))
    expect(variables.types).to(equal((int, float)))
    expect(variables.descriptions).to(equal(["a", "b"]))

    expect(variables[0]).to(equal((0, "a", int)))
    expect(variables[1]).to(equal((1, "b", float)))

    for index_, _ in enumerate(variables):
        if index_ == 0:
            expect(variables[0]).to(equal((0, "a", int)))
        else:
            expect(variables[1]).to(equal((1, "b", float)))

    with pytest.raises(ValueError, match="provide one of"):
        Variables(
            descriptions=["a", "b"],
            types=(int, float),
        )


def test_add():
    variables_1 = Variables(
        indexes=[0, 1],
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )

    variables_2 = Variables(
        descriptions=["a", "b"],
        indexes=(0, 1),
        types=(int, float),
    )
    variables_3 = Variables(
        indexes=[2, 3],
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )

    variables_4 = Variables(
        descriptions=["c", "d"],
        indexes=(2, 3),
        types=(int, float),
        useless_stuff=(),
    )

    with pytest.raises(ValueError, match="but not the other"):
        _ = variables_1 + variables_2

    variables_1plus3 = variables_1 + variables_3
    variables_1 += variables_3
    expect(variables_1plus3).to(equal(variables_1))
    expect(variables_1plus3).to(
        equal(
            Variables(
                indexes=(0, 1, 2, 3),
                descriptions=("a", "b", "a", "b"),
                names=("chien", "chat", "chien", "chat"),
                types=(int, float, int, float),
                useless_stuff=(),
            )
        )
    )

    variables_2plus4 = variables_2 + variables_4
    variables_2 += variables_4
    expect(variables_2plus4).to(equal(variables_2))
    expect(variables_2plus4).to(
        equal(
            Variables(
                indexes=(0, 1, 2, 3),
                descriptions=("a", "b", "c", "d"),
                types=(int, float, int, float),
            )
        )
    )

    variables_5 = Variables(
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
    )
    variables_6 = Variables(
        indexes=[2, 3],
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )

    with pytest.raises(ValueError, match="different reference names"):
        _ = variables_5 + variables_6

    variables_6 = Variables(
        descriptions=["a", "b"],
        names=("chien", "chat"),
        types=(int, float),
        useless_stuff=(),
    )

    with pytest.raises(ValueError, match="unique identifiers here"):
        _ = variables_5 + variables_6

    variables_6 = Variables(
        names=("cheval", "chouette"),
        types=(int, float),
    )

    with pytest.raises(ValueError, match="but not the other"):
        _ = variables_5 + variables_6

    variables_6 = Variables(
        descriptions=["a", "b"],
        names=("cheval", "perdrix"),
        types=(int, float),
        useless_stuff=(),
    )

    variables_5plus6 = variables_5 + variables_6
    variables_5 += variables_6

    expect(variables_5plus6).to(equal(variables_5))
    expect(variables_5plus6).to(
        equal(
            Variables(
                descriptions=["a", "b", "a", "b"],
                names=("chien", "chat", "cheval", "perdrix"),
                types=(int, float, int, float),
            )
        )
    )
