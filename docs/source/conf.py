import os.path
import sys
from pathlib import Path
from datetime import datetime

sys.path.insert(0, os.path.abspath("../../"))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "alloniamodel"
copyright = f"{datetime.now().year}, AllOnIA"
author = "AllOnIA"
source_suffix = [".md", ".rst"]
release = "0.1.0"
# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

rst_prolog = """
.. role:: inlinepython(code)
   :language: python
"""

extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx_mdinclude",
    "nbsphinx",
    "sphinx_gallery.load_style",
    "sphinx_gallery.gen_gallery",
    "sphinx_copybutton",
]

sphinx_gallery_conf = {
    "examples_dirs": "examples/imports",
    "gallery_dirs": "auto_examples",
    "filename_pattern": r".*\.py",
    "plot_gallery": False,
    "download_all_examples": False,
    "compress_images": ("images", "thumbnails"),
}

# nbsphinx_allow_errors = True
nbsphinx_execute = "never"
nbsphinx_prolog = (
    r"""
{% set docname = "../../" + env.doc2path(env.docname, base=None) | string %}
"""
    + Path("prolog.rst").read_text()
)
templates_path = ["_templates"]
nbsphinx_thumbnails = {
    # '*': '_static/jupyter-logo.png',
    "examples/notebooks/email_classification": "_static/spam.png",
    "examples/notebooks/titanic_classification_example": "_static/titanic.png",
    "examples/notebooks/housing_regression": "_static/house.png",
    "examples/notebooks/iris_classification_example": "_static/iris.png",
    "examples/notebooks/mnist_tensorflow_example": "_static/tf.png",
    "examples/notebooks/xgboost_example": "_static/xgboost.png",
    "examples/notebooks/clustering": "_static/clustering.png",
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
exclude_patterns = [
    "prolog.rst",
    "sg_execution_times.rst",
    "auto_examples/*.ipynb",
    "auto_examples/*.py",
]
html_static_path = ["_static"]
html_logo = "allonia-logo.svg"

# html_css_files = [
#     'css/custom.css'
# ]


autodoc_mock_imports = [
    "pandas",
    "numpy",
    "sklearn",
    "public",
    "allonias3",
]
html_context = {"default_mode": "light"}
html_sidebars = {
    "**": [
        "search-field.html",
        "globaltoc.html",
        "relations.html",
        "sourcelink.html",
    ]
}

intersphinx_mapping = {
    "pandas": ("https://pandas.pydata.org/docs/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "sklearn": ("https://scikit-learn.org/stable", None),
    "python": ("https://docs.python.org/3", None),
    "h5py": ("https://docs.h5py.org/en/stable", None),
    "allonias3": ("https://aleia-team.gitlab.io/public/allonias3", None),
}

napoleon_google_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_preprocess_types = False
napoleon_type_aliases = True
napoleon_attr_annotations = True
