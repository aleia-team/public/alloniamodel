"""
Example of service using Iris KNN
---------------------------------

This service creates an API that allows one to predict using the existing Iris
KNN model on a fixed dataset, but using user-specified weights and number of
neihbors.
"""
# sphinx_gallery_thumbnail_path = '_static/iris.png'
import os
import pandas as pd
from flask import Flask, request, Blueprint, make_response
from apispec_webframeworks.flask import FlaskPlugin
from apispec import APISpec
from flask_swagger_ui import get_swaggerui_blueprint

import aleialib


APP_PREFIX = f"/user-services/invoke/{os.environ.get('USER_SERVICE_ID')}"


def register_swagger(app_):
    spec = APISpec(
        title=os.environ.get("USER_SERVICE_NAME"),
        version="1.0.0",
        openapi_version="3.0.2",
        plugins=[FlaskPlugin()],
    )
    swaggerui_blueprint = get_swaggerui_blueprint(
        APP_PREFIX + "/docs", APP_PREFIX + "/docs/specs.json"
    )

    @swaggerui_blueprint.route("/specs.json", methods=["GET"])
    def get_api_doc():
        return spec.to_dict(), 200

    app_.register_blueprint(swaggerui_blueprint)
    with app_.test_request_context():
        for _, view in app_.view_functions.items():
            spec.path(view=view)


user_services_routes = Blueprint(
    "user_services_routes", __name__, url_prefix=APP_PREFIX
)


@user_services_routes.route("/health", methods=["GET"])
def get_health():
    """Get Health

    ---
    get:
      description: Get Health
      responses:
        200:
          description: Returns Health boolean
          content:
            application/json:
              schema:
                type: object
                properties:
                  health:
                    type: boolean
    """
    return make_response({"health": True})


# routes to be written by users
@user_services_routes.route("/predict", methods=["POST"])
def predict():
    """Predicts using trained Iris KNN.

    One can specify how many neighbors to use with which weights.

    ---
    post:
      tags:
        - Predictor
      description: Predicts using AllOnIAModel.
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                weights:
                  type: string
                  required: false
                neighbors:
                  type: integer
                  required: false

      responses:
        200:
          description: Predicted values as a list.
          content:
            application/json:
              schema:
                type: object
                properties:
                  predictions:
                    type: array
                  weights:
                    type: string
                  neighbors:
                    type: integer
        400:
          description: Returns an error message
    """
    request_json = request.get_json()
    fits_summary = pd.DataFrame.from_dict(
        aleialib.user_services.invoke(
            "post",
            "/attribute",
            "52fbf15e-bdfa-477a-9ca6-7f39da042c03",
            {"model_name": "iris_knn", "attribute": "fits_summary"},
        )["fits_summary"]
    )[["revision", "parameters", "results"]].values.tolist()
    params_rev_res = pd.DataFrame(
        [
            (
                params["model_kwargs"]["n_neighbors"],
                params["model_kwargs"]["weights"],
                res[1]["accuracy"],
                revision,
            )
            for revision, params, res in fits_summary
        ],
        columns=["n", "w", "a", "r"],
    )
    params_rev_res = (
        params_rev_res.groupby(["n", "w"])
        .apply(lambda x: x.sort_values("a", ascending=False).iloc[0])
        .drop(columns=["n", "w"])
    )

    weights = request_json.get("weights")
    neighbors = request_json.get("neighbors")

    if (neighbors, weights) not in params_rev_res.index:
        return make_response(
            {
                "error": f"Weights {weights} and neighbors {neighbors} not found in training history"
            },
            400,
        )
    output = aleialib.user_services.invoke(
        "post",
        "/predict",
        "52fbf15e-bdfa-477a-9ca6-7f39da042c03",
        {
            "kwargs": {"feature_engineering_kwargs": {"names": ""}},
            "model_name": "iris_knn",
            "observations_set": {
                "path": "notebooks/dataset/iris_no_target.csv"
            },
            "reload": False,
            "revision": int(params_rev_res.loc[(neighbors, weights), "r"]),
            "save": False,
        },
    )

    # If output is none of those types, make sure it is json-serialisable
    return make_response(
        {"predictions": output, "weights": weights, "neighbors": neighbors}
    )


app = Flask(__name__)
app.register_blueprint(user_services_routes)
register_swagger(app)


if __name__ == "__main__":
    app.run()
