"""
Titanic classification functions
--------------------------------

Functions imported in the `Titanic classification notebook`_.

.. _Titanic classification notebook: ../examples/notebooks/titanic_classification_example.ipynb

"""
# sphinx_gallery_thumbnail_path = '_static/titanic.png'

import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score, confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.ensemble import RandomForestClassifier


class AdaptedRandomForest(RandomForestClassifier):
    """AllOnIAModel will always use `model.predict` to get
    predictions. In the case of classification, we might need to use
    `model.predict_proba` to assess the quality
    of the learning (it returns the predicted probability of each class for
    each observation, instead of the one class with the highest probability).

    We can do that by overloading `sklearn.ensemble.RandomForestClassifier, and
    adding a kwarg to the `model.predict`predict` method. When predicting for
    metrics computation, we can then pass
    `predict_for_metrics_kwargs={"which": "proba"}`, like in the notebook.
    """

    def predict(self, X, which="values"):
        if which == "values":
            return super().predict(X)
        elif which == "proba":
            return super().predict_proba(X)
        else:
            raise ValueError(
                f"Invalid value '{which}' for 'which'. "
                "Can be 'values' or 'proba'"
            )


def getmetrics(predicted_proba, y, model):
    ns_probs = [0 for _ in range(len(y))]
    predicted_proba = predicted_proba[:, 1]
    ns_auc = roc_auc_score(y, ns_probs)
    lr_auc = roc_auc_score(y, predicted_proba)
    cm = pd.DataFrame(
        columns=["dead", "survived"],
        index=["dead", "survived"],
        data=confusion_matrix(
            y.values,
            [0 if p < 0.5 else 1 for p in predicted_proba],
            labels=model.classes,
        ),
    )
    # summarize scores
    print("No Skill: ROC AUC=%.3f" % ns_auc)
    print("Logistic: ROC AUC=%.3f" % lr_auc)
    # calculate roc curves
    ns_fpr, ns_tpr, _ = roc_curve(y, ns_probs)
    lr_fpr, lr_tpr, _ = roc_curve(y, predicted_proba)
    return {
        "AUC": ns_auc,
        "Result": lr_auc,
        "ROC No skill": (ns_fpr, ns_tpr),
        "ROC Logistic": (lr_fpr, lr_tpr),
        "cm": cm,
    }


def TrainTestSplit(dataframe, test_size, random_state):
    split = StratifiedShuffleSplit(
        n_splits=1, test_size=test_size, random_state=random_state
    )
    strat_train_set = None
    strat_test_set = None
    for train_index, test_index in split.split(
        dataframe, dataframe["Survived"]
    ):
        strat_train_set = dataframe.loc[train_index]
        strat_test_set = dataframe.loc[test_index]
    if strat_train_set is None:
        raise ValueError("Could not split dataset.")
    return strat_train_set, strat_test_set


def clean_data_titanic(dataset):
    dataset = dataset.drop(["Ticket", "Cabin"], axis=1).reset_index(drop=True)

    dataset["Title"] = dataset.Name.str.extract(r" ([A-Za-z]+)\.", expand=False)
    dataset["Title"] = dataset["Title"].replace(
        [
            "Lady",
            "Countess",
            "Capt",
            "Col",
            "Don",
            "Dr",
            "Major",
            "Rev",
            "Sir",
            "Jonkheer",
            "Dona",
        ],
        "Rare",
    )

    title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}

    dataset["Title"] = (
        dataset["Title"]
        .replace("Mlle", "Miss")
        .replace("Ms", "Miss")
        .replace("Mme", "Mrs")
        .map(title_mapping)
        .fillna(0)
    )
    dataset = dataset.drop(["Name"], axis=1)
    dataset["Sex"] = dataset["Sex"].map({"female": 1, "male": 0}).astype(int)
    grouped = dataset.groupby(["Sex", "Pclass"])["Age"]
    # Replace the missing ages by the median age of the same sex and social
    # class. Convert random age float to nearest .5 age
    dataset["Age"] = grouped.apply(
        lambda x: x.fillna(round(x.dropna().median()))
    ).astype(int)

    dataset.loc[dataset["Age"] <= 16, "Age"] = 0
    dataset.loc[(dataset["Age"] > 16) & (dataset["Age"] <= 32), "Age"] = 1
    dataset.loc[(dataset["Age"] > 32) & (dataset["Age"] <= 48), "Age"] = 2
    dataset.loc[(dataset["Age"] > 48) & (dataset["Age"] <= 64), "Age"] = 3

    dataset["FamilySize"] = dataset["SibSp"] + dataset["Parch"] + 1

    dataset["IsAlone"] = (dataset["FamilySize"] == 1).values.astype(int)
    dataset = dataset.drop(["Parch", "SibSp", "FamilySize"], axis=1)

    dataset["Age*Class"] = dataset.Age * dataset.Pclass

    freq_port = dataset.Embarked.dropna().mode()[0]
    dataset["Embarked"] = dataset["Embarked"].fillna(freq_port)

    dataset["Embarked"] = (
        dataset["Embarked"].map({"S": 0, "C": 1, "Q": 2}).astype(int)
    )
    dataset["Fare"] = dataset["Fare"].fillna(dataset["Fare"].dropna().median())
    dataset.loc[dataset["Fare"] <= 7.91, "Fare"] = 0
    dataset.loc[
        (dataset["Fare"] > 7.91) & (dataset["Fare"] <= 14.454), "Fare"
    ] = 1
    dataset.loc[
        (dataset["Fare"] > 14.454) & (dataset["Fare"] <= 31), "Fare"
    ] = 2
    dataset.loc[dataset["Fare"] > 31, "Fare"] = 3
    dataset["Fare"] = dataset["Fare"].astype(int)

    return dataset
