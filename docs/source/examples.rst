.. _examples:

##########
 Examples
##########

***********
 Notebooks
***********

The following notebooks provide working examples that you can copy-paste
in AllOnIA Platform's jupyter lab. We recommand exploring them all in the order
they appear below.

.. nbgallery::
   :caption: See some examples in notebooks :

   examples/notebooks/housing_regression.ipynb
   examples/notebooks/email_classification.ipynb
   examples/notebooks/iris_classification_example.ipynb
   examples/notebooks/xgboost_example.ipynb
   examples/notebooks/mnist_tensorflow_example.ipynb
   examples/notebooks/titanic_classification_example.ipynb
   examples/notebooks/clustering.ipynb

.. include:: auto_examples/index.rst