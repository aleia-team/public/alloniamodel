.. _lock:

#################################
 AllOnIAModel lock and read-only
#################################

Imagine you just trained a model and are trying to predict something with it.
In the mean time, your colleague, who does not know what you are doing,
re-trains the model with different parameters. That could mess up your results
and the conclusion you can draw from them.

To avoid that, AllOnIAMdoel objects are locked by default : after doing

.. code-block:: python

    model = AllOnIAModel("monmodele")

No one will be able to open the model "monmodele" until one of the following was
done :

 * **BEST PRACTICE** : :inlinepython:`model.close()` (see :obj:`~alloniamodel.model.AllOnIAModel.close`),
   in the notebook where the :inlinepython:`model` variable is defined
 * :inlinepython:`AllOnIAModel.unlock("monmodele")` (see :obj:`~alloniamodel.model.AllOnIAModel.unlock`).
   This command does not need :inlinepython:`model` to be defined, and can thus be called by anyone anywhere
 * The object :inlinepython:`model` got properly garabge-collected upon program end

You can override this lock by doing

.. code-block:: python

    model = AllOnIAModel("monmodele", override_lock=True)

but be prepared to face the consequences if a colleague was using the model...

You can also choose to open the model in read-only mode. In that case, the lockfile
will be ignored, but you will not be able to call :inlinepython:`model.save()` nor
to persist intermediate data (see :ref:`data_retention`).

See :obj:`~alloniamodel.model.AllOnIAModel.__init__` for more details about the possible
arguments for creating an AllOnIAModel object.


You can see the history of who opened any existing model by calling
:obj:`~alloniamodel.model.AllOnIAModel.get_opening_history` :

.. code-block:: python

    history = AllOnIAModel.get_opening_history("monmodele")

It will return a dictionary looking like :

.. code-block:: json

    {
        "2023-09-23 21:51:16.719006": {
            "open_by": "track ad5c1b3e-71d8-4e6c-9ab8-9c379ae5f20a in project a5b3c901-7cb8-4499-b6ae-fb80891cf90d by user bf90db1b-6991-42ed-b8f6-0a292a471a7a in file 3436793207.py",
            "closed_at": "2023-09-23 21:54:22.058054"
        },
        "2023-09-23 21:53:37.130433": {
            "open_by": "track ad5c1b3e-71d8-4e6c-9ab8-9c379ae5f20a in project a5b3c901-7cb8-4499-b6ae-fb80891cf90d by user bf90db1b-6991-42ed-b8f6-0a292a471a7a in file 1645227127.py"
        },
        "2023-09-23 22:10:59.017767": {
            "open_by": "track ad5c1b3e-71d8-4e6c-9ab8-9c379ae5f20a in project a5b3c901-7cb8-4499-b6ae-fb80891cf90d by user bf90db1b-6991-42ed-b8f6-0a292a471a7a in file 1645227127.py"
        },
        "2023-09-25 09:25:43.374914": {
            "open_by": "track ad5c1b3e-71d8-4e6c-9ab8-9c379ae5f20a in project a5b3c901-7cb8-4499-b6ae-fb80891cf90d by user bf90db1b-6991-42ed-b8f6-0a292a471a7a in file 602023144.py"
        }
    }

Note the absence of *close_at* sometimes : it means that :inlinepython:`model.close()` was not called
and the object was not garbage-collected.
Either the model was opened with :inlinepython:`override_lock=True`, or :inlinepython:`AllOnIAModel.unlock("monmodele")`
was used while the model was still open in a notebook.