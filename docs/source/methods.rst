.. _methods:

##############
 User methods
##############

The user will have to provide his AllOnIAModel with various methods, or even
classes, that he/she will define. Those are the following:

 * :obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_function`
 * :obj:`~alloniamodel.model.AllOnIAModel.train_val_test_split_function`
 * :obj:`~alloniamodel.model.AllOnIAModel.compute_metrics_function`
 * :obj:`~alloniamodel.model.AllOnIAModel.postprocess_function`

Note that none of those functions are mandatory, each of them have a default
behavior. Only
:obj:`~alloniamodel.model.AllOnIAModel.model` and
:obj:`~alloniamodel.model.AllOnIAModel.model_class` are  classes that **must** be
provided (one of them at least), but **do not have** to be made by the user.
They can also be existing sklearn or tensorflow objects.

AllOnIAModel will pickle all the methods that were given to it when
:obj:`~alloniamodel.model.AllOnIAModel.save` is called. When loading the model
again, those methods are readily available. Since
`cloudpickle <https://github.com/cloudpipe/cloudpickle>`_ is used, those methods
can come from imported files or modules: suppose you have the follwing code in
**afile.py** alongside your notebook on S3:

 .. code-block:: python

    def my_awesome_function(df):
        """do stuff on df"""
        ...
        return df_modified

Then in your notebook you can do

 .. code-block:: python

    from allonias3 import S3Path
    S3Path("notebook/afile.py").import_from_s3()
    from afile import my_awesome_function
    from alloniamodel import AllOnIAModel

    model = AllOnIAModel("modelname")
    model.feature_engineering_function = my_awesome_function
    model.save()

Then you could do, in a Module for example (see :ref:`production`):

 .. code-block:: python

    from alloniamodel import AllOnIAModel

    model = AllOnIAModel("modelname")
    model.feature_engineering_function(...)
