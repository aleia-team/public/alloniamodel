.. _monitoring:

####################
 Monitoring a model
####################

****************************
 Retrieving the information
****************************

The following methods automatically record their
:obj:`~alloniamodel.utils.ExecutionMetadata`:

* :obj:`~alloniamodel.model.AllOnIAModel.learn`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.learnings_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.apply`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.applies_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.feature_engineering`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.train_val_test_split_function`, accessible
  through :obj:`~alloniamodel.model.AllOnIAModel.splits_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.train`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.trainings_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.validate`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.validations_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.test`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.tests_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.predict`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.predicts_summary`
* :obj:`~alloniamodel.model.AllOnIAModel.postprocess`, accessible through
  :obj:`~alloniamodel.model.AllOnIAModel.postprocesses_summary`

In the following I will assume that we trained a classification model on
the Iris dataset using a varying number of neighbors, and no other input.
You can find a similar notebook in :ref:`examples`.

The execution metadata learning summaries will output a dataframe of this
format :

.. csv-table:: Learnings summary (scroll -->)
    :delim: ;
    :file: df_1.csv
    :stub-columns: 1
    :header-rows: 1

Duration is in seconds. Here, I assumed that
**new raw data arrived** on S3 between the second and third learning, as
indicated by :inlinepython:`{"raw_set": ("notebooks/dataset/iris.csv", 2)}` in the last line.

 .. warning::
    Deleting a model or a model's revision will **not** update these tables.

*************************
 Reusing the information
*************************

You can load any revision of a model by doing

.. code-block:: python

    from alloniamodel import AllOnIAModel

    model = AllOnIAModel("iris_knn", revision=1)

In that case, if you already did 3 learnings and **if you did**
:obj:`~alloniamodel.model.AllOnIAModel.save`
**between each learning**, the previous line will load the very first learning
you did, and you can predict with it right away.

If you **did not** :obj:`~alloniamodel.model.AllOnIAModel.save` between each learning,
as a revision corresponds to a
saved file on S3, you will only have "1" in the **revision** column of your learning summary.
Then, you can not access the model trained with 1 neighbor, as loading revision
1 will load the model trained with 10 neighbors. However, you can retrain your
model in the same conditions as in the very first learning by asking the model
to train on revision 1 of the raw set again (see :ref:`data_input`).

.. code-block:: python

    from alloniamodel import AllOnIAModel

    model = AllOnIAModel("iris_knn", revision=1)
    model._raw_set.use_revision(1)
    model.learn(model_kwargs={"n_neighbors": 1})
    # To use the latest one again next time you train
    model._raw_set.use_revision(None)
    model.save()

This will create revision 2 with the same learning conditions than the first
learning you did.

Note that :obj:`~alloniamodel.utils.DataHandler.use_revision` is only useful if new data arrived. You can see
it by looking at the column **dataset_revision**.

:obj:`~alloniamodel.utils.DataHandler.use_revision` can be used on any dataset given as anything but :obj:`~alloniamodel.utils.URL`.

********
 Seldon
********

A *model.joblib* file is created alongside the *model.alloniamodel* file, that
contains the latest trained model in joblib format.