#########################
 AllOnIAModel Docstrings
#########################

********************
 AllOnIAModel Class
********************

.. autoclass:: alloniamodel.model.AllOnIAModel
   :members:

************
 Variables
************

.. autoclass:: alloniamodel.variables.Variables
   :members:

************
 Decorators
************

.. automodule:: alloniamodel.decorator
   :members:

*******
 Utils
*******

.. automodule:: alloniamodel.utils
   :members:

**********
 Checkers
**********

.. automodule:: alloniamodel.checker
   :members:

********
 Errors
********

.. automodule:: alloniamodel.errors
   :members:
