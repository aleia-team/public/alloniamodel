.. _pipeline_steps:

##############################
 AI Pipelines in AllOnIAModel
##############################

.. _pipeline_learn:

===================
 Learning pipeline
===================

1. Load raw data by setting :obj:`~alloniamodel.model.AllOnIAModel.raw_set` attribute.
   See :ref:`data_input`.
2. Feature engineering: produces cleaned/derived data from raw data
   using :obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_function`
   (default is the identity
   function). See :ref:`feature_engineering`.
3. Splits the derived data in train, validation and test sets using
   :obj:`~alloniamodel.model.AllOnIAModel.train_val_test_split_function` (default is
   :obj:`~sklearn.model_selection.train_test_split`). More in :ref:`split`
4. Uses :obj:`~alloniamodel.model.AllOnIAModel.predictive_variables` and
   :obj:`~alloniamodel.model.AllOnIAModel.target_variables` to
   define the X and y and passes them to :obj:`~alloniamodel.model.AllOnIAModel.model`'s fit function.
   See :ref:`training`.
5. Evaluates the performance using :obj:`~alloniamodel.model.AllOnIAModel.compute_metrics_function` on
   the validation set and on the test set, if their sizes are not zero. More in :ref:`evaluating`.
6. Sends a mail to all :obj:`~alloniamodel.model.AllOnIAModel.validators` with a summary of the
   learning process. Validators are added through :obj:`~alloniamodel.model.AllOnIAModel.add_validator`.

The user can trigger all those at once by doing

 .. code-block:: python

    model = AllOnIAModel("modelname")
    ...
    model.learn()

or one after the other by doing

 .. code-block:: python

    model = AllOnIAModel("modelname")
    ...
    derived = model.feature_engineering("raw")
    train, validation, test = model.train_validation_test_split()
    model.train()
    validation_metrics = model.validate()
    test_metrics = model.test()
    model._send_summary()

but that is not recommended.

If doing **hyperparameter optimisation**, one should always use
:obj:`~alloniamodel.model.AllOnIAModel.learn`, as it is at the beginning of this
method that :obj:`~alloniamodel.model.AllOnIAModel.model_class` will be called
to define :obj:`~alloniamodel.model.AllOnIAModel.model`.


.. _feature_engineering:

---------------------
 Feature Engineering
---------------------

In AllOnIAModel, feature engineering is an optional method that the user can
define and give to the model by defining the
:obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_function` attribute:

 .. code-block:: python

    def my_awesome_function(df):
        """do stuff on df"""
        ...
        return df_modified

    model = AllOnIAModel("modelname")
    model.feature_engineering_function = my_awesome_function

See :ref:`methods` for more information about custom methods in AllOnIAModel.

It is assumed that the process providing the user with the data to learn
on yields the same format as the process providing the data to predict on, so
by default both the learning and prediction pipelines will execute this
function. The learning on the :obj:`~alloniamodel.model.AllOnIAModel.raw_set` and
the prediction on the
:obj:`~alloniamodel.model.AllOnIAModel.observations_set`. The resulting
datasets are saved in the attribute :obj:`~alloniamodel.model.AllOnIAModel.derived_set`
and :obj:`~alloniamodel.model.AllOnIAModel.derived_observations_set` respectively.
One can skip any of those steps by providing the model with the corresponding
result dataset before running the pipeline.

AllOnIAModel will automatically pass the :obj:`~alloniamodel.model.AllOnIAModel.raw_set`
or :obj:`~alloniamodel.model.AllOnIAModel.observations_set` as the first argument to
the function. Any other argument is optional and must be passed using custom
keyword arguments (see :ref:`custom_keyword`).

The function should *not* modify the input dataset in place, but return a new
one.

As the same method is used for both :obj:`~alloniamodel.model.AllOnIAModel.raw_set`
and :obj:`~alloniamodel.model.AllOnIAModel.observations_set`, it must handle the
presence and the absence of the target variable.

.. _split:

------------------------------
 Train - Validate - Test split
------------------------------

The :obj:`~alloniamodel.model.AllOnIAModel.derived_set` is then passed to this
function, that can be specified to the attribute
:obj:`~alloniamodel.model.AllOnIAModel.train_val_test_split_function`. By default,
:obj:`sklearn.model_selection.train_test_split` is used.

It must accept at least 3 arguments, that AllOnIAModel will pass automatically:
 * The derived set
 * The test or validation set size, as a fraction of the size of the derived set
 * the random state

It should only make ONE split, AllOnIAModel will call it twice to make both
validation and test sets. It should return TWO sets.

The user can change the validation and test sets sizes through
:obj:`~alloniamodel.model.AllOnIAModel.set_set_sizes` : its first argument is the
validation size, the second is the test size. The default values are 0.2 and 0.2
so 20% of the derived set each, leaving 60% for the train set.

Any (and both) of those sizes can be 0, which will result in the pipeline
skipping the validation or test step.

It is the user responsibility to specify sizes that will leave the train set with
enough data to do something.

The user can change the random state through the
:obj:`~alloniamodel.model.AllOnIAModel.random_state` attribute (default is None).

The user can also provide :ref:`custom_keyword`.

.. _training:

----------
 Training
----------

Training will first look for the columns names or indexe numbers that correspond to
the :obj:`~alloniamodel.model.AllOnIAModel.predictive_variables` in the train set,
and by doing so will detect their types if they were not provided by the user.
Then, if existing, it will do the same with the
:obj:`~alloniamodel.model.AllOnIAModel.target_variables`. Those are optional, as
one could be training a clustering model for example.

Those two can be provided through the :obj:`~alloniamodel.model.AllOnIAModel.set_variables`
method, either as two collections of :obj:`int` or :obj:`str`, or as
two :obj:`~alloniamodel.variables.Variables` objects, to provide more information.

If the data in the train set corresponding to the
:obj:`~alloniamodel.model.AllOnIAModel.target_variables` are :obj:`int`,
:obj:`bool` or :obj:`str`, then :obj:`~alloniamodel.model.AllOnIAModel.classes` will
be the set of those values. It can be useful for computing some metrics in a
classification problem.

The user can also choose to reshape the data by doing
:inlinepython:`model.train(reshape_x=(-1, 1), reshape_y=(-1, 1))`. This can be
useful if for example, the targets are a 1-D array, but the model needs a 2-D
shape, like :obj:`sklearn.linear_model.LinearRegression`.

Finaly, the AllOnIAModel object calls its
:obj:`~alloniamodel.model.AllOnIAModel.model` :inlinepython:`fit` method on
predictive and target variables.

:ref:`custom_keyword` can be provided.

.. _evaluating:

------------
 Evaluating
------------

After the learning, the model will compute metrics using the user-defined
:obj:`~alloniamodel.model.AllOnIAModel.compute_metrics_function` on the
validation and the test sets. If it was
not specified, and empty dictionary is returned as metrics.

The function should accept 1 or more arguments: the predicted targets.

  * The predicted targets are given automatically by AllOnIAModel when
    evaluating.
  * If the model is supervised (real targets are known), they are given
    as second argument.
  * If 'x' or 'X' appear in the possible arguments, then the predictive
    variables are passed to the function. It can be any positional
    argument.
  * If it is the first or second argument and the model is supervised,
    the other argument is the real targets, and predicted targets are
    not passed along.
  * Else, the first argument is the predicted targets (and if
    supervised the second is the real targets), and the predictive
    variables are passed as 'X'

Any other argument must be passed as :ref:`custom_keyword` when learning.
It should return a dictionary of metrics, the keys being the metrics names.

.. _pipeline_predict:

=====================
 Prediction pipeline
=====================

1. Load observations by setting :obj:`~alloniamodel.model.AllOnIAModel.observations_set` attribute.
   See :ref:`data_input`.
2. Feature engineering: see :ref:`feature_engineering`.
3. Uses :obj:`~alloniamodel.model.AllOnIAModel.predictive_variables` to define the X to pass to
   :obj:`~alloniamodel.model.AllOnIAModel.model`'s predict function, returns the predicted values.
4. Uses :obj:`~alloniamodel.model.AllOnIAModel.postprocess_function` on predictions and returns its
   result (default is the identity function). It must accept at least one argument,
   and possible :ref:`custom_keyword`.

The user can trigger all those at once by doing

 .. code-block:: python

    model = AllOnIAModel("modelname")
    ...
    model.apply()

or one after the other by doing

 .. code-block:: python

    model = AllOnIAModel("modelname")
    ...
    derived = model.feature_engineering("observed")
    predictions = model.predict()
    post_processed_predictions = model.postprocess()

All the computed predictions can be retrieved via
:obj:`~alloniamodel.model.AllOnIAModel.get_predictions`.