.. _custom_keyword:

##########################
 Custom keyword arguments
##########################

--------------
 Generalities
--------------

Suppose you need to classify Iris, and suppose you want to do it with various
values for :inlinepython:`n_neighbors`. You can then give the following argument to the
:obj:`~alloniamodel.model.AllOnIAModel.learn` method :

.. code-block:: python

    from alloniamodel import AllOnIAModel
    from sklearn.neighbors import KNeighborsClassifier

    model = AllOnIAModel("iris_classif")
    model.model_class = KNeighborsClassifier
    # Other required model attributes
    ...
    for n in (1, 5, 10, 15):
        model.learn(model_kwargs={"n_neighbors": n})
        model.save()

Another example : you created a feature engineering that multiplies some
columns by some values, and want to specify the columns and values at
execution :

.. code-block:: python

    from alloniamodel import AllOnIAModel


    def feat_eng(df, columns=None, values=None):
        if columns is None:
            return df
        df[columns] *= values
        return df


    model = AllOnIAModel("iris_classif")
    model.feature_engineering_function = feat_eng
    # Other required model attributes
    ...
    model.learn(
        feature_engineering_kwargs={"columns": ["col1", "col2"], "values": [10, 20]}
    )
    model.save()

The methods that accept custom kwargs are the following ones, please check their
individual documentation for details:

* :obj:`~alloniamodel.model.AllOnIAModel.health_check`
* :obj:`~alloniamodel.model.AllOnIAModel.learn`
* :obj:`~alloniamodel.model.AllOnIAModel.evaluate`
* :obj:`~alloniamodel.model.AllOnIAModel.apply`

---------------------------
 Giving the Model as value
---------------------------

Suppose you have a custom function that needs the :obj:`~alloniamodel.model.AllOnIAModel`
itself as value for on of the argument, like in `the Titanic classification notebook`_
metric function, where the :inlinepython:`model` argument expects
the :obj:`~alloniamodel.model.AllOnIAModel` object:

.. _the Titanic classification notebook: examples/notebooks/titanic_classification_example.ipynb

 .. code-block:: python

    def getmetrics(predicted_proba, y, model):
        ns_probs = [0 for _ in range(len(y))]
        predicted_proba = predicted_proba[:, 1]
        ns_auc = roc_auc_score(y, ns_probs)
        lr_auc = roc_auc_score(y, predicted_proba)
        cm = pd.DataFrame(
            columns=["dead", "survived"],
            index=["dead", "survived"],
            data=confusion_matrix(
                y.values,
                [0 if p < 0.5 else 1 for p in predicted_proba],
                labels=model.classes,
            ),
        )
        # summarize scores
        print("No Skill: ROC AUC=%.3f" % ns_auc)
        print("Logistic: ROC AUC=%.3f" % lr_auc)
        # calculate roc curves
        ns_fpr, ns_tpr, _ = roc_curve(y, ns_probs)
        lr_fpr, lr_tpr, _ = roc_curve(y, predicted_proba)
        return {
            "AUC": ns_auc,
            "Result": lr_auc,
            "ROC No skill": (ns_fpr, ns_tpr),
            "ROC Logistic": (lr_fpr, lr_tpr),
            "cm": cm,
        }

You can then pass it like that :

 .. code-block:: python

    model = AllOnIAModel(...)
    ... some learning code ...
    test_metrics = model.test(metrics_kwargs={"model": model})

But you could also do the following, which is prefered when using a user service
to which you can pass arguments, but where you do not have the model object
(see :ref:`userservices`) :

 .. code-block:: python

    model = AllOnIAModel(...)
    ... some learning code ...
    test_metrics = model.test(metrics_kwargs={"model": "self"})

All the methods accepting custom kwargs are decorated with
:obj:`~alloniamodel.decorator.convert_self_kwarg`, which will convert
the value :inlinepython:`"self"` to the :obj:`~alloniamodel.model.AllOnIAModel`
object.
