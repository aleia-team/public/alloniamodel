.. _production:

###################
 Put in production
###################

This section supposes you have already successfully trained and saved an
AllOnIAModel, and that you want to predict with it.

There are three ways to put an AllOnIAModel in production on Aleia Platform :

 1. Using `modules, pipelines and jobs`_
 2. Using `user services`_
 3. Using `Seldon`_


.. _modules, pipelines and jobs: auto_examples/iris_knn_module_and_pipeline.rst
.. _user services: auto_examples/service.rst


*****************************
 Modules, Pipelines and Jobs
*****************************

Go to the Platform's **Modules** section and create a new module. Copy the
content of the module example (link in the list at the top of this page)

Then, go to the Platform's **Pipelines** section and create a new pipeline.
Give the path to the input data to **data_note_1**'s *source*, and the path
where the predictions should be stored to **data_note_2**'s *source*.
Change the value of **processor** to match your previously created module's
name.

Save the pipeline and go back to the **Pipelines** section. Click the three
vertical dots on the right of your new pipeline and click **Build a job**. If
you do not see this option, it means you lack the necessary access rights and
need to contact to superior.

Finally, go to the **Jobs** section of the Platform. After a few seconds, your
new job should appear (refresh the page if it does not). Click **Run**. Once
finished, it will move to the **Done** tab, where you can see its logs.

.. _userservices:

***************
 User services
***************

This allows you to create API routes to interact with your model.

Go to the **Services** section of the Platform and create a new service. Edit
its code (you can copy the example linked at the top of this page).
In most cases you will not have to change anything.

From the **Services** section, click on your service's **Deployment** switch.
Wait a few minutes while refreshing the page until the **Health** indicator is
green. Then, open the service's **Documentation**.

 * The **Internal call** will show you how to interrogate your route from inside
   the platform (from a notebook for example).
 * The **External call** will show you how an outside user can interrogate your
   route.
 * The **API Documentation** is a Swagger that will allow you to test your route
   directly.

.. _Seldon:

************************
 Deployment with Seldon
************************

------------
 Using code
------------

You can also give direct access to your model's prediction method using Seldon.

To do that, run the following line:

 .. code-block:: python

    import aleiamlops

    # Deploy a Seldon model using the model name
    ret = aleiamlops.seldon.deploy_model("iris_knn")
    # OR, for a specific revision
    ret = aleiamlops.seldon.deploy_model("iris_knn", revision=2)

    print(ret["url"])

You can do that in a notebook, a job, or a service. But, in a notebook is easier.
Copy url from the returned value.

Then, **inside the same track**, you or your colleague can execute the following lines to predict
with the model:

 .. code-block:: python

    import aleiamlops

    aleiamlops.seldon.list_model("iris_knn")  # If you lost URL :)
    aleiamlops.seldon.model_predict(
        seldon_deployment_url,
        shape=(1, 4),  # The shape of the predictive variables
        values=[[5.3, 3.5, 1.4, 0.2]]  # The predictive variables (here only one line)
    )


Note that a Seldon deployment will create a dedicated pod on the cluster, that
needs resources and might induce some aditional cost. So, it is a good practice
to tear it down if you do not use it:

 .. code-block:: python

    import aleiamlops

    aleiamlops.seldon.delete_model_deployment(seldon_deployment_id)

You can also test it directly in your notebook, like in the Iris classification
example (link at the top of this page).

-----------------------------
 Directly from the platforme
-----------------------------

On the **Models** page, find the trained model you want to deploy, click on the
action menu, then click **Deploy**:

.. image:: _static/deploy.png
  :width: 800
  :alt: Deploy an AllOnIAModel in one click

Choose the environment type you want then click **Confirm**.

Your model should then appear on the **Service** page, in **Model Services**:

.. image:: _static/model_service.png
  :width: 800
  :alt: A deployed AllOnIAModel

Wait for the **Health** icon to turn green (check the logs in the **Actions** menu
if it does not), then use the URL displayed in the **Deployment** column to query
your model.

A futur feature will allow to query the model directly from the platform. For now
you have to do it from a code (see above).
