.. _data:

#################################
 Data management in AllOnIAModel
#################################

.. _data_input:

*********************************
 Give input data to AllOnIAModel
*********************************

=============
 Basic input
=============

For more details about the class handeling the data, see :obj:`~alloniamodel.utils.DataHandler`.

You can find the list of all the datasets that can exist in an AllOnIAModel object at
:obj:`~alloniamodel.model.AllOnIAModel.datasets`. To define one of those, remove the
leading underscore.

Predictions are also datasets, but one should not set them manualy, they
ought to be computed by the model. You can access them via
:obj:`~alloniamodel.model.AllOnIAModel.get_predictions`.

In order to learn anything or predict anything, your model will need data. There
are mainly two ways of giving data to a model, and you should prefer the later :

 1. Give the data as-is (a :obj:`~pandas.DataFrame` object for example)
 2. Give a (list of) path to a S3 file using :obj:`~allonias3.s3_path.S3Path`.
    AllOnIAModel will load its content when needed.

And why should you use the later ? Because AllOnIAModel will remember the path to
the file, so the next time you load your model, you can easily retrieve it. Plus,
you can know which S3 version of the file was used during learning (see
:ref:`monitoring`) and reproduce a previous learning by using a specific
version of the model **and** of the dataset.

:obj:`~pandas.DataFrame` objects however can be heavy, so they are not saved, and will be forgotton
once the notebook is closed. You can change this behaviour though, see :ref:`data_retention`.

When giving a path, you need to include the prefix "notebooks", like this :

.. code-block:: python

    from alloniamodel import AllOnIAModel
    from allonias3 import S3Path

    model = AllOnIAModel(...)
    model.raw_set = S3Path("notebooks/dataset/file.csv")

Calling :inlinepython:`model.raw_set` will then return the file content. This will
work out of the box for :obj:`~pandas.DataFrame` in **.csv** or **.parquet** files,
:obj:`~numpy.ndarray` in **.npy** file or dictnaries in **.json** files. If the file is a **.h5** file,
it will return an open :obj:`h5py.File` object, and it will be up to you to handle it in
your feature engineering function. Any other extension will result in the program trying to, first,
decode it to a string using utf-8, falling back to unpickling it upon failure.

Another thing you can do : you can specify the kwargs to pass to :obj:`~allonias3.base_path.BasePath.read`.
You can give :inlinepython:`{"index_col": None}` for example, if your .csv files do not have an index :

.. code-block:: python

    model = AllOnIAModel(...)
    model.raw_set = S3Path("notebooks/dataset/file.csv")
    model._raw_set.load_kwargs = {"index_col": None}

If you want to force the pickling/unpickling of the data no matter the files extensions, do:

.. code-block:: python

    model = AllOnIAModel(...)
    model.raw_set = S3Path("notebooks/dataset/file.csv")
    model._raw_set.handle_type = False

and :inlinepython:`revision` and :inlinepython:`version` are handled by
:obj:`~alloniamodel.utils.DataHandler.use_revision`.

You can define any intermediary set too, if you do not need your pipeline to
start from scratch (see :ref:`pipeline_steps`) :

.. code-block:: python

    model = AllOnIAModel(...)
    model.train_set = S3Path("notebooks/dataset/file.csv")
    model.train()

In the previous example, the feature engineering and train-validate-test split
are skipped. Note that you can still call :inlinepython:`model.learn()` to execute
the full pipeline then, as it will detect which parts of the pipeline can be executed
based on which datasets are defined, and skip the others.

If you are re-using a model which already defined the path to a set, you can
ask for a specific version of it like this :

.. code-block:: python

    model = AllOnIAModel(...)
    model._raw_set.use_revision(3)


Note the access to the protected :inlinepython:`model._raw_set` instead of using
the property :inlinepython:`model.raw_set`.

================
 Advanced input
================

Sometimes, your data is not simple dataframes or arrays. Theny can be spread across
multiple files, come from an external URL, be of exotic format... Most of the use cases
should be supported by AllOnIAModel, even though it might require some more work on your end.

.. _many_files:

------------
 Many files
------------

To aggregate several :obj:`~pandas.DataFrame` spread accros several files along
their lines, just pass the tuple of file paths to
the model object like that :


.. code-block:: python

    from alloniamodel import AllOnIAModel
    model = AllOnIAModel("monmodele")
    model.raw_set = (
        S3Path("notebooks/dataset/file1.csv"),
        ...
        S3Path("notebooks/dataset/filen.csv"),
    )

This also works with dataframes in .parquet file, or :obj:`~numpy.ndarray` in
**.npy** files.
Note that if you want to aggregate along the columns (each file contains
all the index but only a subset of columns), you will need to do :

>>> from alloniamodel import AllOnIAModel
>>> model = AllOnIAModel("monmodele")
>>> model.raw_set = (
>>>     S3Path("notebooks/dataset/file1.csv"),
>>>     S3Path("notebooks/dataset/file2.csv"),
>>>     S3Path("notebooks/dataset/file3.csv"),
>>> )
>>> model._raw_set.concatenate_kwargs = {"axis": 1}

You can pass any keyword argument in :inlinepython:`concatenate_kwargs` as long as it is
understood by :obj:`~pandas.concat` or :obj:`~numpy.concatenate` (depending on your
use case).

In that case too you can choose which version to use for the files when loading
an existing model. You can either do

.. code-block:: python

    model = AllOnIAModel(...)
    model._raw_set.use_version(3)

In which case all the files will use revision number 3, or

.. code-block:: python

    model = AllOnIAModel(...)
    model._raw_set.use_version((3, 1, None))

In which case *file1.csv* will use revision number 3, *file2.csv* revision number
1 and *file3.csv* the latest revision. you need to specify one revision per file.

.. _data_types_url:

--------------
 External URL
--------------

Use the dedicated class :obj:`~alloniamodel.utils.URL` and give it to the set
as input. See its docstring for more information. Note that in that case, you
can not ask for a specific version of the data. You can force those sets to
re-download the data even if it already has been by doing :

.. code-block:: python

    model = AllOnIAModel(...)
    model.raw_set = URL("https://...")
    model._raw_set.force_download = True

------------------------
 Aggregate complex data
------------------------

If you data is spread across multiple files of different types, or across multiple
URLs, or something else that has not been detailed here, then the best thing you can
do is handle that in the :obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_function`.

---------------------
 Special Data Format
---------------------

If your input file(s) does not contain :obj:`~numpy.ndarray`, :obj:`~pandas.DataFrame` or
:obj:`~pandas.Series`, you will have to provide a basic
:obj:`~alloniamodel.model.AllOnIAModel.feature_engineering_function` to cast your data
either into one of those three formats or, if not possible, in
:obj:`~alloniamodel.utils.SpecialDataFormat`. This class is a very simple wrapper
around any data separated in predictive and target variables through its
:obj:`~alloniamodel.utils.SpecialDataFormat.x` and :obj:`~alloniamodel.utils.SpecialDataFormat.y`
attributes. If you use it, you will also have to provide a custom
:obj:`~alloniamodel.model.AllOnIAModel.train_val_test_split_function` that handles
this format, but you **will not** have to call :obj:`~alloniamodel.model.AllOnIAModel.set_variables`,
as the default values for a model's predictive and target variables are
:inlinepython:`("x",)` and :inlinepython:`("y",)`, and
:obj:`~alloniamodel.utils.SpecialDataFormat` defines :obj:`~alloniamodel.utils.SpecialDataFormat.__getitem__`.
You can see an example in `this notebook`_.

.. _this notebook: examples/notebooks/email_classification.ipynb


.. _data_retention:

********************************
 Data retention in AllOnIAModel
********************************

By default, AllOnIAModel itself never saves any data to S3, be it the raw input
or the intermediary feature engineered computed data. One can however decide
to persist intermediary data to S3 by doing :

.. code-block:: python

    model = AllOnIAModel(...)
    model.save_intermediary_data = True

or

.. code-block:: python

    model = AllOnIAModel(...)
    model.save_intermediary_data = ("raw", "derived")

to save only some intermediary data. All the datasets in
:obj:`~alloniamodel.model.AllOnIAModel.datasets` can be saved that way. To save
predictions, look at the possible dataset names in
:obj:`~alloniamodel.model.AllOnIAModel._valid_set_names` and append
:inlinepython:`"_predictions"` to them. Setting
:inlinepython:`model.save_intermediary_data = True` is equivalent to doing

.. code-block:: python

    model = AllOnIAModel(...)
    model.save_intermediary_data = (
        "raw",
        "derived",
        "train",
        "validation",
        "test",
        "observations",
        "derived_observations",
        "postprocessed_predictions",
        "health_check_set",
        "health_check_observations_set",
        "train_predictions",
        "validation_predictions",
        "test_predictions",
        "observations_predictions"
    )

Doing so will only work if the model is not read-only (see :ref:`lock`).

In the first case, all data computed by AllOnIAModel intermediary pipeline steps
(see :ref:`pipeline_steps`) will be stored in the model's
:obj:`~alloniamodel.model.AllOnIAModel.intermediary_save_path` directory.

In the second case, only the raw set and the feature engineering set are persisted. Note
that the raw set is supposed to be given by the user. If the user gave a S3 path, specifying
:inlinepython:`"raw"` in :inlinepython:`save_intermediary_data` will have no effect :
data given as S3 paths are assumed to
be managed on S3 by the user, and will not be saved by AllOnIAModel. Only the paths will persist in the
pickled AllOnIAModel. If the user gave an object like a :obj:`~numpy.ndarray`, :obj:`~pandas.DataFrame`,
:obj:`~pandas.Series` or :obj:`~alloniamodel.utils.SpecialDataFormat` then it is indeed persisted.

Note that :obj:`~alloniamodel.utils.URL` objects are managed a bit differently : they are always persisted
as links are supposed to be managed by third parties that could update the data without
the user's awareness. See :ref:`data_types_url` for more details.