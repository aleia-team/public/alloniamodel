..
   alloniamodel documentation master file, created by
   sphinx-quickstart on Fri Apr 28 17:42:58 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##########################################
 Welcome to AllOnIAModel's documentation!
##########################################

.. toctree::
   :hidden:
   :caption: Table of Contents:

   examples
   production
   pipeline_steps
   monitoring
   data
   methods
   custom_keywords
   lock
   docstrings

.. mdinclude:: ../../README.md