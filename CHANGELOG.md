## [1.4.7](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.6...1.4.7) (2025-01-14)


### Bug Fixes

* **allonias3:** update to latest ([36e39f9](https://gitlab.com/aleia-team/public/alloniamodel/commit/36e39f942a01678a172e9a321b087a2f3b5554eb))

## [1.4.6](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.5...1.4.6) (2025-01-14)


### Bug Fixes

* **allonias3:** update to latest ([ed0ebb5](https://gitlab.com/aleia-team/public/alloniamodel/commit/ed0ebb5d8857de2daad4cea69da992e291fc3785))

## [1.4.5](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.4...1.4.5) (2024-11-06)


### Bug Fixes

* **allonias3:** update to latest ([ec30635](https://gitlab.com/aleia-team/public/alloniamodel/commit/ec30635877a9ff0ad366fdd3ad873f1bed51b7cd))

## [1.4.4](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.3...1.4.4) (2024-10-30)


### Bug Fixes

* **description:** class -> classes ([8d7ccc8](https://gitlab.com/aleia-team/public/alloniamodel/commit/8d7ccc89952b6ce1f0069355a61bd4f0087a2503))

## [1.4.3](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.2...1.4.3) (2024-10-30)


### Bug Fixes

* **description:** class -> classes ([d5e53ca](https://gitlab.com/aleia-team/public/alloniamodel/commit/d5e53caff3e49ea9dcddd98da65f7f7d11a294bf))

## [1.4.2](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.1...1.4.2) (2024-10-30)


### Bug Fixes

* **description:** Trigger description update upon saving ([0bc98aa](https://gitlab.com/aleia-team/public/alloniamodel/commit/0bc98aa23c9c8cfdffded03a0fe5793c303f3cba))

## [1.4.1](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.4.0...1.4.1) (2024-10-30)


### Bug Fixes

* **description:** learnings were not updated ([6c013bc](https://gitlab.com/aleia-team/public/alloniamodel/commit/6c013bc3b791c3b828389ac37ab91703ff4861ab))

# [1.4.0](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.3.1...1.4.0) (2024-10-29)


### Features

* **description:** put more things into description to avoir unpickling the model. New update_description feature to update the description without unpickling the object. ([bde27e2](https://gitlab.com/aleia-team/public/alloniamodel/commit/bde27e28f1e4568e4f98d47e8773ec666a9de28a))
* **lint:** use future annotations ([0a12863](https://gitlab.com/aleia-team/public/alloniamodel/commit/0a12863f7475b628bfa8ce6c936da3c00688e94c))

## [1.3.1](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.3.0...1.3.1) (2024-10-25)


### Bug Fixes

* **load:** check we are indeed loading an AllOnIAModel ([117fb03](https://gitlab.com/aleia-team/public/alloniamodel/commit/117fb03f540cc0845d7c24e2215f397a14b05674))
* **load:** check we are indeed loading an AllOnIAModel ([1d8e5ba](https://gitlab.com/aleia-team/public/alloniamodel/commit/1d8e5ba5e2d1e3352a478641cad0346fdddc5fdb))

# [1.3.0](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.2.0...1.3.0) (2024-10-24)


### Features

* **allonias3:** update allonias3 ([138e4ec](https://gitlab.com/aleia-team/public/alloniamodel/commit/138e4ec8315e83b127a3bb36f680fa98b2dc629a))

# [1.2.0](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.5...1.2.0) (2024-10-24)


### Features

* **allonias3:** update allonias3 ([3090dad](https://gitlab.com/aleia-team/public/alloniamodel/commit/3090dad1ca186685ba2318ff56824e2c4f15b18c))

## [1.1.5](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.4...1.1.5) (2024-10-23)


### Bug Fixes

* classes handling in description ([bf3ae96](https://gitlab.com/aleia-team/public/alloniamodel/commit/bf3ae96246cc59c4edea3cb6d70d0c8f1c53b7d4))
* classes handling in description ([7264925](https://gitlab.com/aleia-team/public/alloniamodel/commit/7264925190c97dd9982b9d0a4cc130f55f7acb50))
* paths ([4d136b8](https://gitlab.com/aleia-team/public/alloniamodel/commit/4d136b8dcd07372355531aef11ca9e95e7270aaa))

## [1.1.4](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.3...1.1.4) (2024-10-18)


### Bug Fixes

* classes handling in description ([5716363](https://gitlab.com/aleia-team/public/alloniamodel/commit/57163638c3a6da70e4757fd69a85fc07f12374ff))

## [1.1.3](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.2...1.1.3) (2024-10-16)


### Bug Fixes

* logical error in if statement in __new__ ([de34487](https://gitlab.com/aleia-team/public/alloniamodel/commit/de34487cedda9b48c33b821b6e3ca4d50f52ec97))
* missing allow_load_file in __init__ ([7490740](https://gitlab.com/aleia-team/public/alloniamodel/commit/7490740579ca3daf9cb186b8da5210c88f147602))
* upgrade allonias3 ([9604681](https://gitlab.com/aleia-team/public/alloniamodel/commit/9604681d50e553a5faaa94ece936eba2f80e2e30))

## [1.1.2](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.1...1.1.2) (2024-10-11)


### Bug Fixes

* remove 'verbose' in try_delete ([2edb3a1](https://gitlab.com/aleia-team/public/alloniamodel/commit/2edb3a106c5fa1d79c880a32bb4e5cf3abd47801))
* remove 'verbose' in try_delete ([7081501](https://gitlab.com/aleia-team/public/alloniamodel/commit/7081501a4cba90c9ec19f8c727b37e6ce368f5b0))
* remove 'verbose' in try_delete ([91e10b5](https://gitlab.com/aleia-team/public/alloniamodel/commit/91e10b58ad850104458f85ea5c5b834b767d176c))

## [1.1.2](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.1...1.1.2) (2024-10-11)


### Bug Fixes

* remove 'verbose' in try_delete ([91e10b5](https://gitlab.com/aleia-team/public/alloniamodel/commit/91e10b58ad850104458f85ea5c5b834b767d176c))

## [1.1.1](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.1.0...1.1.1) (2024-10-10)


### Bug Fixes

* datatypehandler ([128ffb9](https://gitlab.com/aleia-team/public/alloniamodel/commit/128ffb9268eada84613200e5d45e896ec7ecfd09))

# [1.1.0](https://gitlab.com/aleia-team/public/alloniamodel/compare/1.0.0...1.1.0) (2024-10-10)


### Features

* latest allonias3, print docu link when importing ([924c7dc](https://gitlab.com/aleia-team/public/alloniamodel/commit/924c7dcbf72cfb38c404a0d5404e1ff49caa4726))

# 1.0.0 (2024-09-18)


### Bug Fixes

* almost done ([5cae028](https://gitlab.com/aleia-team/public/alloniamodel/commit/5cae028c5ba0a0a2dbf49240876b194d99812cc3))
* CI ok, doc not ok ([1480960](https://gitlab.com/aleia-team/public/alloniamodel/commit/1480960c4de860118ea1b031e11a9d07a5d7b681))
* doc ok ([62e7b74](https://gitlab.com/aleia-team/public/alloniamodel/commit/62e7b7482f18f2b98fc1528820e99273a8d33e8c))
* docstring ([d65037b](https://gitlab.com/aleia-team/public/alloniamodel/commit/d65037ba198d570ee12d574bb44cefa38a632fb3))
* latest allonias3, env in conftest. Working on making minio mock work ([d465f41](https://gitlab.com/aleia-team/public/alloniamodel/commit/d465f4145513f425e2e63fb597e9d7d199bbec67))
* lots of things. Still a lot to do. ([c2824da](https://gitlab.com/aleia-team/public/alloniamodel/commit/c2824da8fb84383f54e09d2db7c11b10c33d5125))
* lots of things. Still a lot to do. ([e70f4c6](https://gitlab.com/aleia-team/public/alloniamodel/commit/e70f4c6777bebce574bdb872ddba1a555e5de5f2))
* pytests are ok ! ([4aa165f](https://gitlab.com/aleia-team/public/alloniamodel/commit/4aa165fc77ed41b1011bcfa308a506dc7ed05f5b))
* test_first_column_as_index ok ([b7e1af4](https://gitlab.com/aleia-team/public/alloniamodel/commit/b7e1af472a761ceaecbf15a9205837bfda2f05f8))
* test_give_data_as_raw ok ([d1e945d](https://gitlab.com/aleia-team/public/alloniamodel/commit/d1e945d50d52ecc07f2119e74a0eaf9270a209e7))
* test_give_data_input_as_path ok ([0b1c033](https://gitlab.com/aleia-team/public/alloniamodel/commit/0b1c033b09dc2d97befa86011cee75d11f6ca391))
